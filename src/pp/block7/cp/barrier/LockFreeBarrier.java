package pp.block7.cp.barrier;

import java.util.concurrent.atomic.AtomicInteger;

public class LockFreeBarrier implements Barrier {
    AtomicInteger totalWaiting;
    int height;
    public LockFreeBarrier(int height) {
        this.height = height;
        this.totalWaiting = new AtomicInteger(0);
    }
    
    @Override
    public int await() throws InterruptedException {
        // increment number waiting threads
        totalWaiting.incrementAndGet();
        try{
            // return the arrival index
            return totalWaiting.get();
        }
        finally {
            // wait for barrier to trip
            while (totalWaiting.get() == height) {
                Thread.sleep(500);
            }
            // make barrier reusable
            totalWaiting.compareAndSet(height,0);
        }
    }
}
