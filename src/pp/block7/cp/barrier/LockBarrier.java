package pp.block7.cp.barrier;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class LockBarrier implements Barrier {

    AtomicInteger totalWaiting;
    AtomicBoolean tripped;
    int height;

    public LockBarrier(int height) {
        this.height = height;
        this.totalWaiting = new AtomicInteger(0);
        this.tripped = new AtomicBoolean(false);
    }

    @Override
    public synchronized int await() throws InterruptedException {
        totalWaiting.incrementAndGet();
        tripped.compareAndSet(false,totalWaiting.get() == height);
        try{return totalWaiting.get();}
        finally {
            if (!tripped.get()) {
                wait();
            } else {
                notifyAll();
                totalWaiting.getAndSet(0);
                tripped.getAndSet(false);
            }
        }
    }
}
