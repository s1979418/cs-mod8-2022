package pp.block7.cp.lockfree;

import java.util.concurrent.atomic.AtomicReference;

public class LockFreeStack<T> {
    AtomicReference<Node <T>> head = new AtomicReference<>();
    private class Node<T> {
        final T value;
        Node<T> next;
        public Node(T value) {
           this.value = value;
        }
    }

    /**
     * Pushes element onto stack. 
     */
    public void push(T x) {
        Node<T> newHead = new Node<>(x);
        Node<T> oldHead;
        do{
            oldHead = head.get();
            newHead.next = oldHead;
        } while (!head.compareAndSet(oldHead,newHead));
    }

    /**
     * Removes and returns top element from stack. 
     * When stack is initially empty, (busy) waits
     * until an element is there. 
     */
    public T pop() {
        Node <T> oldHead;
        Node <T> newHead;
        do{
            oldHead = head.get();
            if (oldHead == null){
                return null;
            }
            newHead = oldHead.next;
        } while (!head.compareAndSet(oldHead,newHead));
        return oldHead.value;
    }

    /**
     * Indicates if stack is currently empty. 
     */
    public boolean isEmpty() {
        return head.get() == null;
    }
}
