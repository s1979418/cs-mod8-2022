use std::sync::{Arc, Mutex};
use std::thread;



pub(crate) fn main() {
   let a = [6;100]; // initialise a to array with 100 times 6

   let sum = Arc::new(Mutex::new(0));

   let suma = Arc::clone(&sum);
   let sumb = Arc::clone(&sum);
   let th1 = thread::spawn(move|| {
      for i in 0 ..50 {
         let mut sum = suma.lock().unwrap();
         *sum += a[i];
      }
   });
   let th2 =thread::spawn(move || {
      for i in 50 .. 100 {
         let mut sum = sumb.lock().unwrap();
         *sum += a[i];
      }
   });

   th1.join().unwrap();
   th2.join().unwrap();
   println!("{}",*sum.lock().unwrap());
}