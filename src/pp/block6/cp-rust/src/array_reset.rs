use std::sync::{Arc, Mutex};
use std::thread;



pub fn main() {
   let mut a = Arc::new(Mutex::new(Vec::new())); // initialise a to array with 100 times 6

   let a1 = Arc::clone(&mut a);
   let th1 =thread::spawn(move || {
      for _ in 0 ..50 {
         let mut r = a1.lock().unwrap();
         r.push(0);
      }
   });

   let a2 = Arc::clone(&mut a);
   let th2 = thread::spawn(move || {
      for _ in 50 .. 100 {
         let mut r = a2.lock().unwrap();
         r.push(0);
      }
   });
   th1.join().unwrap();
   th2.join().unwrap();
   let a = a.lock().unwrap();
   println!("{:?}`",a);
}