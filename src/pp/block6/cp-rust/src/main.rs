mod move_semantics1;
mod move_semantics2;
mod move_semantics3;
mod closures;
mod threads;
mod array_sum;
mod array_reset;

fn main() {
    move_semantics1::main();
    move_semantics2::main();
    move_semantics3::main();
    closures::main();
    threads::main();
    array_sum::main();
    array_reset::main();
}
