use std::os::linux::raw::stat;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;
use std::time::Duration;

struct IntWrapper {
    value: i32,
}

pub fn main() {
    let mut status = Arc::new(Mutex::new(IntWrapper { value: 0 }));
    let printstat = Arc::clone(&mut status);
    thread::spawn(move || {
        for _ in 0..10 {
            thread::sleep(Duration::from_millis(250));
            status.lock().unwrap().value += 1;
        }
    });
    while printstat.lock().unwrap().value < 10 {
        println!("waiting... ");
        thread::sleep(Duration::from_millis(500));
    }
}