package pp.block6.cc;

import org.junit.Test;
import pp.iloc.Assembler;
import pp.iloc.Simulator;
import pp.iloc.model.Program;
import pp.iloc.parse.FormatException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class CC6E7Test {
    @Test(timeout = 1000)
    public void testFib() {
        Program p = parse();
        Simulator sim = new Simulator(p);
        sim.setIn(new ByteArrayInputStream(String.valueOf(in).getBytes()));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        sim.setOut(out);
        sim.run();
        if (SHOW) {
            System.out.println(p.prettyPrint());
        }
        assertEquals("Result: "+expected, out.toString().trim());
    }
    Program parse() {
        File file = new File("cc6-7" + ".iloc");
        if (!file.exists()) {
            file = new File(BASE_DIR + "cc6-7" + ".iloc");
        }
        try {
            return Assembler.instance().assemble(file);
        } catch (FormatException | IOException e) {
            fail(e.getMessage());
            return null;
        }
    }

    private final static String BASE_DIR = "src/pp/block6/cc/";
    private final static boolean SHOW = true;
    private final static int[] fib = new int[]{1,1,2,3,5, 8,13,21,34,55, 89,144,233,377,610, 987,1597,2584,4181,6765, 10946};
    private final static int in = 20;
    private final static int expected = fib[in];



}
