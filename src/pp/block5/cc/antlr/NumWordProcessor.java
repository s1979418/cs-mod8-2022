package pp.block5.cc.antlr;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import pp.block5.cc.ErrorListener;
import pp.block5.cc.ParseException;

/** Prettyprints a (number, word)-sentence and sums up the numbers. */
public class NumWordProcessor extends NumWordBaseVisitor<Integer> {
	public static void main(String[] args) {
		NumWordProcessor grouper = new NumWordProcessor();
		if (args.length == 0) {
			process(grouper, "1sock2shoes 3 holes");
			process(grouper, "3 strands 10 blocks 11 weeks 15 credits");
			process(grouper, "1 2 3");
		} else {
			for (String text : args) {
				process(grouper, text);
			}
		}
	}

	private static void process(NumWordProcessor grouper, String text) {
		try {
			System.out.printf("Processing '%s':%n", text);
			int result = grouper.group(text);
			System.out.println("Total: " + result);
		} catch (ParseException exc) {
			exc.print();
		}
	}

	/** Groups a given sentence and prints it to stdout.
	 * Returns the sum of the numbers in the sentence.
	 */
	public int group(String text) throws ParseException {
		CharStream chars = CharStreams.fromString(text);
		ErrorListener listener = new ErrorListener();
		Lexer lexer = new NumWordLexer(chars);
		lexer.removeErrorListeners();
		lexer.addErrorListener(listener);
		TokenStream tokens = new CommonTokenStream(lexer);
		NumWordParser parser = new NumWordParser(tokens);
		parser.removeErrorListeners();
		parser.addErrorListener(listener);
		ParseTree tree = parser.sentence();
		listener.throwException();
		return visit(tree);
	}

	@Override public Integer visitSentence(NumWordParser.SentenceContext ctx) {
		int sum = 0;
		String print = "";
		int wordnums = ctx.getChildCount();

		for(int i = 0; i<wordnums;i++){
			ParseTree child = ctx.getChild(i);
			if(child.getClass() == NumWordParser.NumberContext.class){
				//if the child node is a number, add it to the sum and string
				Integer num = visitNumber((NumWordParser.NumberContext) child);
				sum = sum + num;
				print = print + num + " ";
			}
			else{
				if(i == wordnums - 4){
					//if this is the second to last word add an and after the word
					print = print +child.getText()+" and ";
				} else if (i == wordnums -2) {
					//if we are on the last word (last child is always eof) add only the word
					print = print + child.getText();
					break;

				} else{
					//otherwise add a , after the word
					print = print+child.getText()+", ";
				}
			}
		}
		System.out.println(print);
		return sum;
	}

	@Override public Integer visitNumber(NumWordParser.NumberContext ctx) {
		return Integer.parseInt(ctx.NUMBER().getText());
	}

	@Override public Integer visitWord(NumWordParser.WordContext ctx) {
		return 0;
	}

	// Override the visitor methods.
	// Each visitor method should call visit(child)
	// if and when it wants to visit that child node.
}
