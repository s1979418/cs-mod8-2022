package pp.block1.cc.dfa;

public class MyChecker implements Checker{
    @Override
    public boolean accepts(State start, String word) {
        char [] letters = word.toCharArray();
        for (char letter : letters) {
            if (!start.hasNext(letter)){
                return false;
            }
            start = start.getNext(letter);

        }
        return start.isAccepting();
    }

}
