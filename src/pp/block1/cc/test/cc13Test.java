package pp.block1.cc.test;

import org.junit.Assert;
import org.junit.Test;

import pp.block1.cc.antlr.cc13;

public class cc13Test {
    private static LexerTester tester = new LexerTester(cc13.class);

    @Test
    public void quotesTest() {
        // 0 or >1 ids in succession
        tester.yields("");
        tester.yields("Laaaaaaaa     ", 1);
        tester.yields("LaLaLaLi",3);
        tester.yields("La La", 2);
        tester.yields("La La La Li ", 3);
        tester.yields("Laaaa Laaaa", 2);
        tester.wrong("Lii");
        tester.yields("La La La La La Li", 2,3);

    }
}
