package pp.block1.cc.test;

import org.junit.Assert;
import org.junit.Test;

import pp.block1.cc.antlr.*;

public class cc12Test {
    private static LexerTester tester = new LexerTester(cc12.class);

    @Test
    public void quotesTest() {
        // 0 or >1 ids in succession
        tester.yields("");
        tester.yields("\"The quotation mark, \"\", should be typeset in italics.\"",1);
    }
}
