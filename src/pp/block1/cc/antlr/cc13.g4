lexer grammar cc13;
fragment LAX: 'La';
fragment LI: 'Li';
fragment SPACE: ' '+;
fragment A:'a'+;

LA: LAX A? SPACE?;
LALA: LA LA;
LALI : LA LA LA LI SPACE?;