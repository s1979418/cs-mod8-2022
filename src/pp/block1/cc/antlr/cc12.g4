lexer grammar cc12;
//In PL/I, the programmer can insert a quotation mark into a string by
  //writing two quotation marks in a row. Thus, the string
  //The quotation mark, ", should be typeset in italics
  //would be written in a PL/I program as
  //"The quotation mark, "", should be typeset in italics."
  //Design an RE and an FA to recognize PL/I strings. Assume that strings
  //begin and end with quotation marks and contain only symbols drawn
  //from an alphabet, designated as 6. Quotation marks are the only
  //special case.
  fragment LETTER : ('a'..'z'|'A'..'Z');
  fragment ALPHANUM : ('0'..'9'|LETTER);
  fragment QUOTE : '"';

  QUOTEMARK: QUOTE  (~'"' | QUOTE QUOTE)* QUOTE;