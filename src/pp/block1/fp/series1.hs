{-# LANGUAGE TemplateHaskell #-}
import Test.QuickCheck
import Data.Char
import Data.List

--ex 1.1
f :: Int -> Int
f x = 2*(x*x)+3*x-5

--ex 1.2
total1 :: Int -> Int
total1 0 = 0
total1 n = total1 (n-1) + n

total2 :: Int -> Int
total2 n = (n * (n+1)) `div` 2

total_equal :: Int -> Bool
total_equal n = total1 n == total2 n

prop_total :: Int -> Property
prop_total n = (n >= 0) ==> total1 n == total2 n

--ex 1.3
--Check if plus communicative
communicativeplus :: Int -> Int -> Bool
communicativeplus x y = (x + y) == (y + x)

--Check if minus communicative
communicativeminus :: Int -> Int -> Bool
communicativeminus x y = (x - y) == (y - x)

--ex 1.4
-- Shift a character 3 spaces in the alphabet
cycle_alpha :: Char -> Char
cycle_alpha a
    | a <= 'W' && a >= 'A'  = chr((ord a)+3)
    | a >= 'X' && a <= 'Z'  = chr(((((ord a)+3)-65)`mod`26)+65)
    | a <= 'w' && a >= 'a'  = chr((ord a)+3)
    | a >= 'x' && a <= 'z'  = chr(((((ord a)+3)-97)`mod`26)+97)
    | otherwise             = a

-- Shift a character n spaces in the alphabet
cycle_alpha_times :: Char -> Int -> Char
cycle_alpha_times a n
    | a >= 'A' && a <= 'Z'      = chr((((ord a)-65) + n`mod`26)`mod`26 + 65)
    | a >= 'a' && a <= 'z'      = chr((((ord a)-97) + n`mod`26)`mod`26 + 97)
    | otherwise             = a

--test shifting funtions
prop_cycle_alpha :: Char -> Bool
prop_cycle_alpha a = cycle_alpha a == cycle_alpha_times a 3

prop_cycle_alpha_return :: Char -> Int -> Bool
prop_cycle_alpha_return a n = (a == (cycle_alpha_times (cycle_alpha_times a n) (26-n)))

--ex 1.5
--Calculate interest recursively
interest :: Float -> Float -> Int -> Float
interest a r 0 = a
interest a r y = r/100 * (interest a r (y-1)) + interest a r (y-1)

--ex 1.6
-- solve ax^2 + bx + c = 0 using discriminant
root1 :: Float -> Float -> Float -> Float
root1 a b c 
    | a == 0 = error "a may not be 0"
    | discr a b c >= 0 = (-b + sqrt(discr a b c))/(2*a)
    | discr a b c < 0  = error "negative discriminant"

root2 :: Float -> Float -> Float -> Float
root2 a b c 
    | a == 0 = error "a may not be 0"
    | discr a b c >= 0 = (-b - sqrt(discr a b c))/(2*a)
    | discr a b c < 0  = error "negative discriminant"

discr :: Float -> Float -> Float -> Float
discr a b c = (b*b) - 4*a*c

--ex 1.7
-- solve for extreme value ax^2 + bx + c
extrX :: Float -> Float -> Float
extrX a b   = -b / 2*a

extrY :: Float -> Float -> Float -> Float
extrY a b c = a*((extrX a b)^2) + b* (extrX a b) + c

--ex 1.8
--recursive list length
mylength :: [a] -> Int
mylength [] = 0
mylength (x:xs) = 1+ mylength xs

prop_length :: [a] -> Bool
prop_length xs = mylength xs == length xs

--recursive list sum
mysum :: [Int] -> Int
mysum [] = 0
mysum (x:xs) = x + (mysum xs)

prop_sum :: [Int] -> Bool
prop_sum xs = mysum xs == sum xs

--recursive list order reverse
myreverse:: [a] -> [a]
myreverse [] = []
myreverse xs = (last xs) : myreverse (init xs)

prop_reverse :: Eq a => [a] -> Bool
prop_reverse xs = myreverse xs == reverse xs

--take first n elements of a list
mytake :: [a] -> Int -> [a]
mytake as 0 = []
mytake [] n = []
mytake (x:xs) n 
    | n < 0 = []
    | n >= 0 = x: (mytake xs (n-1))
    | n < 0 = error "n cannot be negative"

prop_take :: Eq a => [a] -> Int -> Bool
prop_take xs n = mytake xs n == take n xs

--determine if a given element is in a list
myelem :: Eq a => [a] -> a -> Bool
myelem [] n = False
myelem (x:xs) n
    | x == n = True
    | otherwise = myelem xs n

prop_elem :: Eq a => [a] -> a -> Bool
prop_elem xs n = myelem xs n == elem n xs

--concatenate lists
myconcat :: [a] -> [a] -> [a]
myconcat [] xs = xs
myconcat xs ys = myconcat (init xs) (last xs : ys)

prop_concat :: Eq a => [a] -> [a] -> Bool
prop_concat xs ys = (myconcat xs ys) == (xs ++ ys)

--find the maximum in a list of numbers
mymaximum :: Ord a => [a] -> a
mymaximum [] = error "empty list"
mymaximum [x] = x
mymaximum (x:xs)
    | x > mymaximum xs = x
    | x <= mymaximum xs = mymaximum xs

prop_max :: Ord a => [a] -> Bool
prop_max xs = mymaximum xs == maximum xs

--make 2 lists into a list of tuples
myzip :: [a] -> [b] -> [(a,b)]
myzip [] ys = []
myzip xs [] = []
myzip (x:xs) (y:ys) = (x,y):(myzip xs ys)

prop_zip :: Eq a => [a] -> [a] -> Bool
prop_zip xs ys = myzip xs ys == zip xs ys

--ex 1.9
-- recursively make an arithmatic sequence
r :: Num a => a -> a -> [a]
r a d = [a] ++ r (a+d) d

-- get the nth number from an arithmatic sequence
r1 :: Num a => a -> a-> Int -> a 
r1 a d n = (r a d) !! n

-- sum the ith until jth element from an arithmatic sequence
totalr :: Num a => a -> a-> Int -> Int -> a 
totalr a d i j 
    | i == j = r1 a d i
    | (i+1) == j = r1 a d i + r1 a d j
    | otherwise = r1 a d i + r1 a d j + r1 a d (i+1) + r1 a d (j-1)

--ex 1.10
-- determine whether all elements in a list are equal
allEqual :: Eq a => [a] -> Bool
allEqual [] = True
allEqual [a] = True
allEqual [a,b] = a==b
allEqual (x:y:xs) = x == y && allEqual (y:xs)

-- determine whether a list is an arithmatic sequence
isAs :: Num a => Eq a => [a] -> Bool
isAs [] = True
isAs [a] = True
isAs [a,b] = True
isAs [a,b,c] = (b + (b-a)) == c
isAs (x:y:z:xs) = (y + (y-x)) == z && isAs (y:z:xs)

--ex 1.11
-- check all rows in matrix eqqual length
allRowsEquallyLong :: [[a]] -> Bool
allRowsEquallyLong [] = True
allRowsEquallyLong [a] =True
allRowsEquallyLong [a,b] = length a == length b
allRowsEquallyLong (x:y:xs) = length x == length y && allRowsEquallyLong (y:xs)

-- sum every row in the matrix
rowTotals :: (Foldable t, Num a) => [t a] -> [a]
rowTotals [] = []
rowTotals [a] = [sum a]
rowTotals (x:xs) = [sum x] ++ rowTotals xs

-- transpose the matrix, every nth row becomes every nth column
mytranspose :: [[a]] -> [[a]]
mytranspose [] = []
mytranspose [[]] = []
mytranspose ([]:xs) = mytranspose xs
mytranspose xs = (map head (filter (not . null) xs)) : (mytranspose (map tail (filter (not . null) xs)))

prop_transpose :: Eq a => [[a]] -> Bool
prop_transpose xs = mytranspose xs == transpose xs


-- sum the columns of the matrix
colTotals :: Num a => [[a]] -> [a]
colTotals [] = []
colTotals [[]] = []
colTotals [a] = a
colTotals xs = (sum (map head (filter (not . null) xs))) : (colTotals (filter (not . null) (map tail  xs)))



return []
check = $quickCheckAll