{-# LANGUAGE TemplateHaskell #-}
import Test.QuickCheck
import Data.Char
import Data.List

--ex 1.12
-- own implementation filter
myfilter :: (a -> Bool) -> [a] -> [a]
myfilter _ [] = []
myfilter f [x]
    | f x == True = [x]
    | f x == False = []
myfilter f (x:xs) 
    | f x == True = x : (myfilter f xs)
    | f x == False = myfilter f xs

prop_filter :: Eq a => Fun a Bool -> [a] -> Bool
prop_filter (Fun _ f) xs = myfilter f xs == filter f xs

-- own implementation foldl
myfoldl :: (a -> a -> a) -> a -> [a] -> a
myfoldl f s [] = s
myfoldl f s [x] = f s x
myfoldl f s [x,y] = f (f s x) y
myfoldl f s (x:xs) = myfoldl f (f s x) xs

prop_foldl :: Eq a => Fun (a, a) a -> a -> [a] -> Bool
prop_foldl (Fn2 f) s xs = myfoldl f s xs == foldl f s xs

-- own implementation foldr
myfoldr :: Num a => (a -> a -> a) -> a -> [a] -> a
myfoldr f s [] = s
myfoldr f s [x] = f x s
myfoldr f s (x:xs) = f x (myfoldr f s xs)

prop_foldr :: Num a => Eq a => Fun (a, a) a -> a -> [a] -> Bool
prop_foldr (Fn2 f) s xs = myfoldr f s xs == foldr f s xs

-- own implementation zipWith
myzipWith :: (a -> a -> a) -> [a] -> [a] -> [a]
myzipWith f [] [] = []
myzipWith f xs [] = []
myzipWith f [] ys = []
myzipWith f (x:xs) (y:ys) = (f x y):(myzipWith f xs ys)

prop_zipWith ::Eq a => Fun (a, a) a -> [a] -> [a] -> Bool
prop_zipWith (Fn2 f) xs ys = myzipWith f xs ys == zipWith f xs ys

--ex 1.13
-- make a data type
data Person = Person String Int Char String deriving (Show)

-- example database
people = [Person "Tessa" 20 'F' "Enschede", 
            Person "Kiona" 22 'F' "Enschede", 
            Person "Ivo" 20 'M' "Hengelo",
            Person "Wietske" 52 'F' "Goor",
            Person "Floor" 40 'F' "Sweden",
            Person "Remco" 32 'M' "Enschede",
            Person "Lisa" 35 'F' "Purmerend"]

-- get functions
name :: Person -> String
name (Person n _ _ _) = n

age :: Person -> Int
age (Person _ a _ _) = a

sex :: Person -> Char
sex (Person _ _ s _) = s

residence :: Person -> String
residence (Person _ _ _ r) = r

-- using list comprehension and recursion age change
ageChange :: [Person] -> Int -> [Person]
ageChange [] n = []
ageChange (x:xs) n = Person (name x) ((age x) +n) (sex x) (residence x) : (ageChange xs n)

-- using list comprehension age change
ageChangeList :: [Person] -> Int -> [Person]
ageChangeList xs n = [(Person (name x) ((age x)+n) (sex x) (residence x))| x <- xs]

-- using higher order functions age change
ageChangeH :: [Person] -> Int -> [Person]
ageChangeH xs n = map (\x -> Person (name x) ((age x) +n) (sex x) (residence x)) xs

-- using recursion find all women between 30 and 40
women3040Rec :: [Person] -> [String]
women3040Rec [] = []
women3040Rec (x:xs)
    | (age x) >= 30 && (age x) <= 40 && (sex x) == 'F' = name x : (women3040Rec xs)
    | otherwise = women3040Rec xs

-- using list comprehension find all women between 30 and 40
women3040List :: [Person] -> [String]
women3040List xs = [(name x) | x <- xs , age x <= 40 && age x >= 30 && sex x == 'F']

-- using higher order functions find all women between 30 and 40
women3040H :: [Person] -> [String]
women3040H xs = map name (filter (\x -> ((age x) >= 30 && (age x) <= 40 && (sex x) == 'F')) xs)

-- find a person by their name and give their age
findName :: [Person] -> String -> Int
findName [] n = 0
findName [x] n 
    | (map toUpper (name x)) == (map toUpper n) = age x 
    | otherwise = 0
findName (x:xs) n 
    | (map toUpper (name x)) == (map toUpper n) = age x
    | otherwise = findName xs n

-- sort database by age
sortAge :: [Person] -> [Person]
sortAge [] = []
sortAge xs = [(Person w x y z)| (x,w,y,z) <- sort [(x,w,y,z)|(Person w x y z)<-xs]] 

--ex 1.14
-- implement sieve of erathostenes
tosieve = [2, 3..]
sieve :: [Int] -> [Int]
sieve [] = []
sieve (x:xs) = x:(sieve (filter (\y -> not (y `mod` x == 0)) xs))

-- determine if a number is prime
isprime :: Int -> Bool
isprime n = elem n (sieve [2,3..(n+1)])

-- deliver first n prime numbers
takeprime :: Int -> [Int]
takeprime n = take n (sieve [2,3..])

-- deliver all prime numbers smaller than n
smallprime :: Int -> [Int]
smallprime n = [x | x <- (sieve [2,3..n])]

-- define a function dividers that yields the list of all dividers of a given natural number m. Using
-- this function, define an alternative function to determine whether a given number is prime
dividers :: Int -> [Int]
dividers 0 = []
dividers 1 = []
dividers n = [x | x <- [1,2..n], n `mod` x == 0 && x < n]

isprimediv :: Int -> Bool
isprimediv n = (dividers n) == [] || (dividers n) == [1]

--ex 1.15
-- generate pythagorean triples 
pyth :: Int -> [(Int, Int, Int)]
pyth n = [(a,b,c) | a <- [1,2..n], b <- [1,2..n], c <- [1,2..n], (a*a) + (b*b) == (c*c) ]

--ex 1.16
--Write a function increasing which checks whether a list is increasing
increasing :: Ord a => Num a => [a] -> Bool
increasing [] = True
increasing [a] = True
increasing [a,b] = (a <= b)
increasing (x:y:xs) 
    | x <= y = increasing (y:xs)
    | x > y = False

-- A list is weakly increasing if every next number in the list is greater than the mean of all previous
--numbers. Write a function weaklyIncreasing which checks whether a list is weakly increasing
weaklyIncreasing :: [Int] -> Bool
weaklyIncreasing [] = True
weaklyIncreasing [a] = True
weaklyIncreasing (x:xs) = weaklyHelp [x] xs

weaklyHelp :: [Int] -> [Int] -> Bool
weaklyHelp [] [] = True
weaklyHelp [] [a] = True
weaklyHelp prev [] = True
weaklyHelp prev [x] = x > ((sum prev)`div`(length prev))
weaklyHelp prev (x:xs)
    | x >= ((sum prev)`div`(length prev)) = weaklyHelp (prev ++ [x]) xs
    | otherwise = False


--ex 1.17
--Write a function sublist that checks whether a list xs is a sublist of a list ys
sublist :: Eq a => [a] -> [a] -> Bool
sublist [] [] = True
sublist xs [] = False
sublist [] ys = True
sublist (x:xs) (y:ys) 
    | length (x:xs) > length (y:ys) = False
    | x == y = sublistHelp xs ys
    | otherwise = sublist (x:xs) ys

sublistHelp :: Eq a => [a] -> [a] -> Bool
sublistHelp [] [] = True
sublistHelp xs [] = False
sublistHelp [] ys = True
sublistHelp (x:xs) (y:ys) 
    | x == y = sublistHelp xs ys 
    | otherwise = False


--Write a function sublist that checks whether a list xs is a partial sublist of a list ys
partsublist :: Eq a => [a] -> [a] -> Bool
partsublist [] [] = True
partsublist xs [] = False
partsublist [] ys = True
partsublist (x:xs) (y:ys) 
    | length (x:xs) > length (y:ys) = False
    | x == y = partsublist xs ys 
    | otherwise = partsublist (x:xs) ys 

--ex 1.18
--Write an implementation for bubble sort
bubblesort :: Eq a => Ord a => [a] -> [a]
bubblesort [] = []
bubblesort [a] = [a]
bubblesort (x:xs) 
    | x > (head xs) = bubblesort (bubble x xs)
    | otherwise = bubble x (bubblesort xs)

bubble :: Eq a => Ord a => a -> [a] -> [a]
bubble n [] = [n]
bubble n (x:xs) 
    | n > x = x:(bubble n xs)
    | otherwise = (n:x:xs)

prop_bsort :: [Int] -> Bool
prop_bsort xs = bubblesort xs == sort xs

--Write an implementation of min max sort 
--takes the minimum and the maximum of a list and puts them in front and
--at the end (respectively). Repeating this process for the list from which the minimum and the
--maximum are removed, will sort the list.

minmaxsort :: Ord a => [a] -> [a]
minmaxsort [] = []
minmaxsort [a] = [a]
minmaxsort xs
    | all (== head xs) xs = xs
    | otherwise = minimum xs : ((minmaxsort (delete (minimum xs) (delete (maximum xs) xs )) ) ++ [(maximum xs)])

prop_maxsort :: Ord a => [a] -> Bool
prop_maxsort xs = minmaxsort xs == sort xs

--Write an implementation of insertion sort
isort :: Ord a => [a] -> [a]
isort [] = []
isort (x:xs) = ins x (isort xs)

ins :: Ord a => a -> [a] -> [a]
ins a [] = [a]
ins a [b]
    | a > b = [b, a]
    | otherwise = [a, b]
ins a (x:xs) 
    | a <= x = a:x:xs
    | a > x = x: (ins a xs)

prop_insort :: Ord a => Eq a => [a] -> Bool
prop_insort xs = (isort xs) == (sort xs)


--Write an implementation of merge sort
msort :: Ord a => [a] -> [a]
msort [] = []
msort [a] = [a]
msort xs = merge (msort (take ((length xs) `div` 2) xs)) (msort (drop ((length xs) `div` 2) xs))

merge :: Ord a => [a] -> [a] -> [a]
merge [] [] = []
merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys)
    | x < y = x:(merge xs (y:ys))
    | x == y = x:y:(merge xs ys)
    | otherwise = y:(merge (x:xs) ys)

prop_mersort :: Ord a => Eq a => [a] -> Bool
prop_mersort xs = msort xs == sort xs

--Write an implementation of quick sort
qsort :: Ord a => [a] -> [a]
qsort [] = []
qsort (x:xs) = (qsort (filter (< x) xs)) ++ (filter (== x) (x:xs)) ++ (qsort (filter (> x) xs))

prop_qsort :: Ord a => Eq a => [a] -> Bool
prop_qsort xs = qsort xs == sort xs


--ex 1.19
-- The function myflip receives a function that receives two arguments, and transforms it
-- to a new function that receives its arguments in reversed order. This means it receives a function of type
-- a -> b -> c and transforms it into a function of type b -> a -> c. For example (the parentheses may
-- be omitted): (myflip take) "abc" 2 == "ab"

-- Use a lambda expression to define the function myflip. Explain to the teaching assistant why using a
-- lambda expression is preferred here
-- Because it more clearly reflects how the function is flipped but no arguments for the 
-- function are given to flip


myflip:: (a->b->c) -> (b->a->c)
myflip f = \x y -> f y x 

prop_flip :: Eq c => Fun (a,b) c -> a -> b -> Bool
prop_flip (Fn2 f) a b = f a b == (flip (myflip f)) a b

--ex 1.20
-- Use both the point-free style and function composition to define the function
-- transform :: String -> String that:
-- • Reverses the string,
-- • Removes all nonalphanumerical character,1-CC Compiler Construction 19
-- • Converts all alphanumerical characters to upper case.
transform :: String -> String
transform = map toUpper . filter isLetter . reverse 


-- Hint: consider the functions toUpper and isLetter.
-- Verify: transform "Hello, world!" == "DLROWOLLEH" 

return []
check = $quickCheckAll