{-# LANGUAGE TemplateHaskell #-}
import Test.QuickCheck
import Data.Char
import Data.List
import Data.Maybe
import FPPrac.Trees

--data RoseTree = RoseNode String [RoseTree]

--ex 2.1
data Tree1a = Leaf1a Int
    | Node1a Int Tree1a Tree1a
    deriving (Show, Eq)

exampleTree1a :: Tree1a
exampleTree1a = (Node1a 1 
                    (Node1a 2 (Leaf1a 4) (Leaf1a 5)) 
                    ((Node1a 3 
                    (Node1a 6 (Leaf1a 8) (Leaf1a 9))  
                    (Node1a 7 (Leaf1a 10) (Leaf1a 11)))))


--Define a function pp1a (for “pre-processor”) that translates a tree of type Tree1a into a tree of type
--RoseTree.
pp1a :: Tree1a ->RoseTree
pp1a (Leaf1a x) = RoseNode (show x) []
pp1a (Node1a x tree1 tree2) = RoseNode (show x) [pp1a tree1, pp1a tree2]

--Define a type Tree1b for binary trees that contain 2-tuples of Ints at the internal nodes and at the
--leaves. 
data Tree1b = Leaf1b (Int, Int)
    | Node1b (Int,Int) Tree1b Tree1b
    deriving (Show, Eq)

exampleTree1b :: Tree1b
exampleTree1b = (Node1b (0,1) 
                    (Node1b (2,3) (Leaf1b (6,7)) (Leaf1b (8,9))) 
                    ((Node1b (4,5) 
                    (Node1b (2,3) (Leaf1b (6,7)) (Leaf1b (8,9)))  
                    (Node1b (4,5) (Leaf1b (8,9)) (Leaf1b (10,11))))))

--Define a function pp1b that transforms trees of type Tree1b into trees of type RoseTree
pp1b :: Tree1b -> RoseTree
pp1b (Leaf1b (x,y)) = RoseNode ((show x)++","++(show y)) []
pp1b (Node1b (x,y) tree1 tree2) = RoseNode ((show x)++","++(show y)) [pp1b tree1, pp1b tree2]

--Define a type Tree1c for binary trees that contains Ints at the leaves and no information at the
--internal nodes (hint: use empty strings in your rose trees).
data Tree1c = Leaf1c Int
    | Node1c Tree1c Tree1c
    deriving (Show, Eq)

exampleTree1c :: Tree1c
exampleTree1c = (Node1c 
                    (Node1c (Leaf1c 1) (Leaf1c 2))
                    (Node1c 
                        (Node1c (Leaf1c 3) (Leaf1c 4))
                        (Node1c (Leaf1c 5) (Leaf1c 6))
                    )
                )

-- Define a function pp1c that transforms trees of type Tree1b into trees of type RoseTree.
pp1c :: Tree1c -> RoseTree
pp1c (Leaf1c x) = RoseNode (show x) []
pp1c (Node1c tree1 tree2) = RoseNode " " [pp1c tree1, pp1c tree2]

--Finally, define the type Tree1d that has 2-tuples of Ints at the leaves, and no information at the
--internal nodes. It should be possible that a tree of this type has any number of subtrees in its internal
--nodes. 

data Tree1d = Leaf1d (Int,Int)
    | Node1d [Tree1d]
    deriving (Show, Eq)

exampleTree1d ::Tree1d
exampleTree1d = (Node1d [
                    Node1d [
                        Node1d [
                            Leaf1d (1,2), Leaf1d (3,4), Leaf1d (5,6)
                        ],
                        Leaf1d (7,8)
                    ],
                    Node1d [
                        Leaf1d (9,10), 
                        Node1d [
                            Leaf1d (11,12)
                        ]
                    ],
                    Leaf1d (13,14),
                    Leaf1d (15,16),
                    Node1d [
                        Leaf1d (17,18)
                    ]
                ])

--Define a function pp1d that transforms trees of type Tree1d into trees of type RoseTree.
pp1d :: Tree1d -> RoseTree
pp1d (Leaf1d (x,y)) = RoseNode ((show x) ++ "," ++ (show y)) []
pp1d (Node1d trees) = RoseNode " " (map pp1d trees)

--Add the following type class definition to your Haskell file:
class PP a where
    pp :: a -> RoseTree

--Add instances for this type class for: Tree1a, Tree1b, Tree1c and Tree1d
instance PP Tree1a where
    pp = pp1a

instance PP Tree1b where
    pp = pp1b

instance PP Tree1c where
    pp = pp1c

instance PP Tree1d where
    pp = pp1d

--ex 2.2
--Define a function treeAdd that adds a number x to every number in a tree of type Tree1a.
treeAdd :: Tree1a -> Int -> Tree1a
treeAdd (Leaf1a x) n = Leaf1a (x+n)
treeAdd (Node1a x tree1 tree2) n = Node1a (x+n) (treeAdd tree1 n) (treeAdd tree2 n)

--Define a function treeSquare that squares every number in a tree of type Tree1a
treeSquare :: Tree1a -> Tree1a
treeSquare (Leaf1a x) = Leaf1a (x*x)
treeSquare (Node1a x tree1 tree2) = Node1a (x*x) (treeSquare tree1) (treeSquare tree2)

--Define a function mapTree 
--that applies a function f of type Int -> Int to every number in a tree of type Tree1a. 
mapTree :: (Int -> Int) -> Tree1a -> Tree1a
mapTree f (Leaf1a x) = Leaf1a (f x)
mapTree f (Node1a x tree1 tree2) = Node1a (f x) (mapTree f tree1) (mapTree f tree2)

--Define the functions treeAdd and treeSquare in terms of mapTree.
treeAddMap :: Tree1a -> Int -> Tree1a
treeAddMap tree n = mapTree (+n) tree

prop_treeAddMap :: Int -> Bool
prop_treeAddMap n = treeAdd exampleTree1a n == treeAddMap exampleTree1a n

treeSquareMap :: Tree1a -> Tree1a
treeSquareMap tree = mapTree (^2) tree

--Define a function addNode that replaces every 2-tuple in a tree of type Tree1b by the sum of the
--numbers in each 2-tuple
addNode :: Tree1b -> Tree1a
addNode (Leaf1b (x,y)) = Leaf1a (x+y)
addNode (Node1b (x,y) tree1 tree2) = Node1a (x+y) (addNode tree1) (addNode tree2)

--Define a function mapTree1b such that a function f :: (Int,Int) -> Int can be applied to every
--2-tuple in a tree of type Tree1b. Demonstrate your function with several binary operations (e.g.,
--addition and multiplication) - hint: use lambda abstraction.
mapTree1b :: ((Int,Int)->Int) -> Tree1b -> Tree1a
mapTree1b f (Leaf1b (x,y)) = Leaf1a (f (x,y))
mapTree1b f (Node1b (x,y) tree1 tree2) = Node1a (f (x,y)) (mapTree1b f tree1) (mapTree1b f tree2)

--ex 2.3
--Define a function binMirror1a that mirrors a tree of type Tree1a. Check (manually)
--if mirroring twice results in the original tree again.
binMirror1a :: Tree1a -> Tree1a
binMirror1a (Leaf1a x) = Leaf1a x
binMirror1a (Node1a x tree1 tree2) = Node1a x (binMirror1a tree2) (binMirror1a tree1)

bincheck :: Bool
bincheck = binMirror1a (binMirror1a exampleTree1a) == exampleTree1a

--Define a type class BinMirror that offers an interface to a function binMirror. Create an instance
--of this type class for Tree1a
class BinMirror a where
    binMirror :: a -> a

instance BinMirror Tree1a where
    binMirror = binMirror1a

--Make an instance of BinMirror for Tree1d such that also all tuples at the leaves will be swapped.
instance BinMirror Tree1d where
    binMirror (Leaf1d (x,y)) = Leaf1d (y,x)
    binMirror (Node1d trees) = Node1d (map binMirror (reverse trees))

--ex 2.4
--In this exercise we use trees with Ints at the internal nodes and nothing at the leaves.
--Define a type TreeInt for such trees
data TreeInt = LeafInt
    | NodeInt Int TreeInt TreeInt
    deriving (Show, Eq)

sortedTreeInt :: TreeInt
sortedTreeInt = NodeInt 10 
                    (NodeInt 5 (
                        NodeInt 3 (
                            NodeInt 2 (
                                NodeInt 1 (LeafInt) (LeafInt)
                                ) (LeafInt)
                            )
                            (NodeInt 4 (LeafInt) (LeafInt)
                            )
                        )
                        
                        (NodeInt 7 (
                            NodeInt 6 (LeafInt) (LeafInt)
                            )
                            (NodeInt 9 (LeafInt) (LeafInt))
                        )
                    )
                    (NodeInt 12 (
                        NodeInt 11 (LeafInt) (LeafInt)
                        )
                        (NodeInt 13 (LeafInt) (LeafInt))
                    )

unsortedTreeInt :: TreeInt
unsortedTreeInt = NodeInt 13 
                    (NodeInt 2 (
                        NodeInt 3 (
                            NodeInt 5 (
                                NodeInt 1 (LeafInt) (LeafInt)
                                ) (LeafInt)
                            )
                            (NodeInt 9 (LeafInt) (LeafInt)
                            )
                        )
                        
                        (NodeInt 11 (
                            NodeInt 6 (LeafInt) (LeafInt)
                            )
                            (NodeInt 4 (LeafInt) (LeafInt))
                        )
                    )
                    (NodeInt 10 (
                        NodeInt 7 (LeafInt) (LeafInt)
                        )
                        (NodeInt 8 (LeafInt) (LeafInt))
                    )
                

--Write a function insertTree that inserts a number in a sorted tree of type TreeInt. Hint: it is
--practical to insert a number at a leaf.
insertTree :: TreeInt -> Int -> TreeInt
insertTree (LeafInt) n = NodeInt n (LeafInt) (LeafInt) 
insertTree (NodeInt x tree1 tree2) n
    | x > n = NodeInt x (insertTree tree1 n) tree2
    | otherwise = NodeInt x tree1 (insertTree tree2 n)

--Write a function makeTree that produces a sorted tree from an unsorted list of numbers, by using
--the function insertTree.
--Write your function in two ways: by recursion, and by foldl or foldr.
makeTreel :: [Int] -> TreeInt
makeTreel [] = LeafInt
makeTreel [a] = NodeInt a (LeafInt) (LeafInt)
makeTreel (x:xs) = foldl insertTree (makeTreel [x])  xs

makeTreer :: [Int] -> TreeInt
makeTreer [] = LeafInt
makeTreer [a] = NodeInt a (LeafInt) (LeafInt)
makeTreer (x:xs) = foldr (flip insertTree) (makeTreer [x]) xs 

--So I can visualise TreeInts
ppInt :: TreeInt -> RoseTree
ppInt (LeafInt) = RoseNode " " []
ppInt (NodeInt x tree1 tree2) = RoseNode (show x) [(ppInt tree1),(ppInt tree2)]

instance PP TreeInt where
    pp = ppInt

--Write a function makeList that delivers the list of all numbers in a tree. If that tree is sorted, the
--list should maintain the sorting.
makeList :: TreeInt -> [Int]
makeList (LeafInt) = []
makeList (NodeInt x tree1 tree2) = (makeList tree1)++[x]++(makeList tree2)

--Combine the functions above to sort a list. Use QuickCheck to verify this function by using the
--sort function from Data.List as a reference.
treeSort :: [Int] -> [Int]
treeSort = makeList.makeTreel

prop_sortTree :: [Int] -> Bool
prop_sortTree xs = treeSort xs == sort xs

--The converse of 4: combine the functions to sort a tree of type TreeInt.
sortTree :: TreeInt -> TreeInt
sortTree = makeTreel.makeList

--ex 2.5 
subtreeAt :: TreeInt -> Int -> Maybe TreeInt
subtreeAt (LeafInt) n = Nothing
subtreeAt (NodeInt x tree1 tree2) n
    | x > n = subtreeAt tree1 n 
    | x < n = subtreeAt tree2 n
    | x == n = Just (NodeInt x tree1 tree2)

--ex 2.6
cutOffAt :: TreeInt -> Int -> TreeInt
cutOffAt (LeafInt) n = LeafInt
cutOffAt (NodeInt x tree1 tree2) 0 = LeafInt
cutOffAt (NodeInt x tree1 tree2) n = NodeInt x (cutOffAt tree1 (n-1)) (cutOffAt tree2 (n-1))

--ex 2.7
data BinTree a = Leaf 
            | Node a (BinTree a) (BinTree a)
            deriving (Show, Eq)

--Make BinTree an instance of PP
instance (Show a) => PP (BinTree a) where
    pp (Leaf) = RoseNode "" []
    pp (Node x tree1 tree2) = RoseNode (show x) [(pp tree1), (pp tree2)]

--Make BinTree an instance of BinMirror
instance BinMirror (BinTree a) where 
    binMirror (Leaf) = Leaf
    binMirror (Node a tree1 tree2) = Node a (binMirror tree2) (binMirror tree1)

--make BinTree and instance of Functor
instance Functor BinTree where
    fmap f (Leaf) = Leaf
    fmap f (Node x tree1 tree2) = Node (f x) (fmap f tree1) (fmap f tree2)

--ex 2.8
data MyList a = Nil | Cons a (MyList a)
            deriving (Show, Eq)

mylst :: MyList Integer
mylst = Cons 1 $ Cons 2 $ Cons 3 $ Nil

--make MyList and instance of Functor
instance Functor MyList where
    fmap f Nil = Nil
    fmap f (Cons x list) = Cons (f x) (fmap f list)

--Define a function fromList :: [a] -> MyList a that converts a list to a MyList
fromList :: [a] -> MyList a
fromList [] = Nil
fromList (x:xs) = Cons x (fromList xs)

--Use QuickCheck to test if for your functor the first functor law holds (note: you may want to
--restrict this test to MyList Int). Hint: let QuickCheck generate a list and use fromList to convert
--it.
prop_myList_functor :: Show b => Eq b => Fun Integer b -> [Integer] -> Bool
prop_myList_functor (Fn f) xs = fmap f (fromList xs) == fromList (fmap f xs)


--ex 2.9
data Person = Person {name :: String ,
                        age :: Integer, 
                        sex :: Char,
                        residence :: String}
            deriving (Show)

-- example database
people = [Person "Tessa" 20 'F' "Enschede", 
            Person "Kiona" 22 'F' "Enschede", 
            Person "Ivo" 20 'M' "Hengelo",
            Person "Wietske" 52 'F' "Goor",
            Person "Floor" 40 'F' "Sweden",
            Person "Remco" 32 'M' "Enschede",
            Person "Lisa" 35 'F' "Purmerend"]

--Use the record update syntax to define a function plus :: Int -> [Person] -> [Person], which
--increases the age of all persons by n years.
plus :: Integer -> [Person] -> [Person]
plus n [] = []
plus n (x:xs) = x {age = age x + n} : (plus n xs)

--Give a function that gives all names in a gives list of Persons.
names :: [Person] -> [String]
names [] = []
names (x:xs) = name x : (names xs)

--ex 2.10
--Use getLine combined with the IO functor to define an
--IO action
--that reads an Integer. 

getInt :: IO Integer
getInt = fmap read getLine

return []
check = $quickCheckAll