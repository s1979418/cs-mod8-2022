{-# LANGUAGE TemplateHaskell #-}
import Test.QuickCheck
import Data.Char
import Data.Maybe
import Data.List

--ex 2.11
data BinTree a b = Leaf b
            | Node a (BinTree a b) (BinTree a b)
            deriving (Show, Eq)

--lookahead of 1 token
la :: [Char] -> [Char] -> Bool
la xs l = length xs > 0 && (head xs) `elem` l

data Value = Const Int
            | Id String
            deriving Show

parseFactor :: [Char] -> (BinTree Char Value, [Char])
parseFactor [] = error "no number input detected"
parseFactor (x:xs)
            | isNumber x = (Leaf (Const (digitToInt x)), xs)
            | isLetter x = (Leaf (Id [x]), xs)
            | x == '(' && la ys ")" = (t1 , tail ys)
            | otherwise = error "not valid input"
            where (t1, ys) = parseExpr xs

parseTerm :: [Char] -> (BinTree Char Value , [Char])
parseTerm xs 
        | la ys "*" = (Node '*' t1 t2 , zs)
        | otherwise = (t1, ys)
        where (t1, ys) = parseFactor xs --Factor on the left
              (t2, zs) = parseTerm $ tail ys -- the '*' operator


parseExpr :: [Char] -> (BinTree Char Value, [Char])
parseExpr xs 
        | la ys "+" = (Node '+' t1 t2 , zs)
        | otherwise = (t1,ys)
        where (t1, ys) = parseTerm xs
              (t2, zs) = parseExpr $ tail ys 


--ex 2.13
data Token = Number Integer
         | Identifier String
         | LBrack
         | RBrack
         | Plus
         | Times
         deriving (Show, Eq)

tokenizer :: String -> [Token]
tokenizer "" = []
tokenizer (' ':xs) = tokenizer xs
tokenizer ('(':xs) = LBrack : tokenizer xs
tokenizer (')':xs) = RBrack : tokenizer xs
tokenizer ('+':xs) = Plus : tokenizer xs
tokenizer ('*':xs) = Times : tokenizer xs

tokenizer xs
            | isNumber $ head xs = Number (read num) : tokenizer nrem
            | isLetter $ head xs = Identifier (word) : tokenizer wrem
            | otherwise = error "invalid input"
            where (num, nrem) = span isNumber xs 
                  (word, wrem) = span isLetter xs


--ex 2.14
parseFactor' :: [Token] -> (BinTree Char Token, [Token])
parseFactor' [] = error "no number input detected"
parseFactor' (Number n :xs) = (Leaf (Number n), xs)
parseFactor' (Identifier i : xs) = (Leaf (Identifier i), xs)
parseFactor' (x : xs) 
            | x == LBrack  =  (t1 , tail ys)
            | otherwise = error "non valid input"
            where (t1, ys) = parseExpr' xs

parseTerm' :: [Token] -> (BinTree Char Token , [Token])
parseTerm' xs 
        | length ys > 0 && head ys == Times = (Node '*' t1 t2 , zs)
        | otherwise = (t1, ys)
        where (t1, ys) = parseFactor' xs --Factor on the left
              (t2, zs) = parseTerm' $ tail ys -- the '*' operator


parseExpr' :: [Token] -> (BinTree Char Token, [Token])
parseExpr' xs 
        | length ys > 0 && head ys == Plus = (Node '+' t1 t2 , zs)
        | otherwise = (t1,ys)
        where (t1, ys) = parseTerm' xs
              (t2, zs) = parseExpr' $ tail ys 


--ex 2.15
evalTree:: BinTree Char Token -> [(String, Integer)] -> Integer
evalTree (Leaf (Number x)) valuelist = x
evalTree (Leaf (Identifier i)) valuelist = fromMaybe 0 $ lookup i valuelist
evalTree (Node '+' tree1 tree2) valuelist = (evalTree tree1 valuelist ) + (evalTree tree2 valuelist)
evalTree (Node '*' tree1 tree2) valuelist = (evalTree tree1 valuelist ) * (evalTree tree2 valuelist)
evalTree tree valuelist = error "invalid tree"



eval :: String -> [(String, Integer)] -> Integer
eval input valuelist = evalTree t valuelist
                    where (t, leftover) = parseExpr' $ tokenizer input

return []
check = $quickCheckAll