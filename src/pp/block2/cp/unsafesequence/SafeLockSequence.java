package pp.block2.cp.unsafesequence;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SafeLockSequence implements Sequence{

    private int num;
    public Lock numLock;

    public SafeLockSequence (){
        this.num = 0;
        this.numLock = new ReentrantLock();
    }
    @Override
    public int getNext() {
        numLock.lock();
        this.num = num + 1;
        int returner = this.num;
        numLock.unlock();
        return returner;

    }
}
