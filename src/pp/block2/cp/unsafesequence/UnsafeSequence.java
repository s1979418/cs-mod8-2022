package pp.block2.cp.unsafesequence;

public class UnsafeSequence implements Sequence{
private int num;

    public UnsafeSequence() {
        this.num = 0;
    }

    @Override
    public int getNext() {
        return num++;
    }
}
