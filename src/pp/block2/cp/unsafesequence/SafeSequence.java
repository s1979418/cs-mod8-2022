package pp.block2.cp.unsafesequence;

import java.util.concurrent.atomic.AtomicInteger;

public class SafeSequence implements  Sequence{
private int num;

    public SafeSequence() {
        this.num = 0;
    }

    @Override
    public int getNext() {
        synchronized (this) {
            num = num +1;
            return num;
        }
    }
}
