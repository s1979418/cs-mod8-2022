package pp.block2.cp.queue;

import net.jcip.annotations.ThreadSafe;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Implementation of the LinkedList{@link Queue} which uses coarse grained locking.
 */
@ThreadSafe
public class FineGrainedLinkedListQueue<T> implements Queue<T> {
    public Lock headLock = new ReentrantLock();
    public Lock tailLock = new ReentrantLock();
    /**
     * The head of the linked list queue.
     * Points to the first element of the list.
     */
    private volatile LinkedListNode<T> head = null;
    /**
     * The tail of the linked list queue.
     * Points to the last element of the list.
     */
    private volatile LinkedListNode<T> tail = null;
    /**
     * Length of the list, cached for performance.
     */
    private final AtomicInteger length = new AtomicInteger();

    @Override
    public void push(T x) {
        LinkedListNode<T> temp = new LinkedListNode<>(x);
        try {
            headLock.lock();
            if (this.head == null) {
                this.head = temp;
            }
        } finally {
            headLock.unlock();
        }
        tailLock.lock();
        if (this.tail == null) {
            this.tail = temp;
        } else {
            this.tail.setNext(temp);
        }
        this.length.incrementAndGet();
        tailLock.unlock();


    }

    @Override
    public T pull() throws QueueEmptyException {

        headLock.lock();

        if (this.head != null) {
            T x = this.head.getContent();
            tailLock.lock();
            LinkedListNode<T> n = null;
            try {
                n = this.head.getNext();

            } catch (NullPointerException e) {
                System.out.print("only 1 element in queue");
            }
            this.head = n;
            this.length.decrementAndGet();
            tailLock.unlock();
            headLock.unlock();

            return x;
        } else {
            headLock.unlock();
            System.out.print(" No elements in the queue ");
            throw new QueueEmptyException();

        }


    }

    @Override
    public int getLength() {
        return this.length.get();
    }
}
