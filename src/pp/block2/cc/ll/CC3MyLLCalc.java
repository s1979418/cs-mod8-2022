package pp.block2.cc.ll;

import pp.block2.cc.NonTerm;
import pp.block2.cc.Symbol;
import pp.block2.cc.Term;

import java.util.*;

public class CC3MyLLCalc implements LLCalc {
    Grammar g;
    Set<NonTerm> nonTerms;
    Set<String> nonTermString;
    Map<String, NonTerm> nonTermTranslate;
    Set<Term> terms;
    Set<String> termString;

    Set<String> nonTermsTerms;

    Map<String, Term> termTranslate;

    public CC3MyLLCalc(Grammar g) {
        this.g = g;
        nonTerms = g.getNonterminals();
        terms = g.getTerminals();
        termString = new HashSet<>();
        termTranslate = new HashMap<>();
        nonTermsTerms = new HashSet<>();
        for (Term t : terms) {
            termString.add(t.getName());
            termTranslate.put(t.getName(), t);
            nonTermsTerms.add(t.getName());
        }
        nonTermString = new HashSet<>();
        nonTermTranslate = new HashMap<>();
        for (NonTerm n : nonTerms) {
            nonTermString.add(n.getName());
            nonTermTranslate.put(n.getName(), n);
            nonTermsTerms.add(n.getName());
        }


    }

    @Override
    public Map<Symbol, Set<Term>> getFirst() {
        Map<Symbol, Set<Term>> returnFirst = new HashMap<>();
        Map<Symbol, Set<Symbol>> oldfirsts = new HashMap<>();
        List<Rule> rules = g.getRules();
        Map<Symbol, Set<Symbol>> firsts = new HashMap<>();
        firsts.put(rules.get(0).getLHS(), null);
        while (!oldfirsts.equals(firsts)) {
            //update the old state
            oldfirsts.putAll(firsts);
            for (Rule r : rules) {

                List<Symbol> rhs = r.getRHS();
                //get the first symbol of the rhs
                Set<Symbol> firstsRule = getRHSWithNt(firsts, r, rhs);
                firsts.put(r.getLHS(), firstsRule);

            }
            Set<Symbol> firstLHS = new HashSet<>(firsts.keySet());
            for (Symbol s : firstLHS) {
                //filter out the non-terminals and add their firsts to the first of the associated symbol
                Set<Symbol> sFirsts = new HashSet<>(firsts.get(s));
                for (Symbol nt : sFirsts) {
                    if (nonTermString.contains(nt.getName())) {
                        Set<Symbol> newFirsts = new HashSet<>();
                        firsts.get(s).remove(nt);
                        newFirsts.addAll(firsts.get(s));
                        newFirsts.addAll(firsts.get(nt));
                        firsts.put(s, newFirsts);
                    }
                }
                //add the terminals to the list
                Set<Term> returnTerms = new HashSet<>();
                for (Symbol f : firsts.get(s)) {
                    if (f != null && !nonTermString.contains(f.getName())) {
                        //don't add any non-terminals to the first set
                        if (f == Symbol.EMPTY) {
                            returnTerms.add((Term) f);
                        } else {
                            returnTerms.add(termTranslate.get(f.getName()));
                        }
                    }
                }
                returnFirst.put(s, returnTerms);
            }
        }
        return returnFirst;
    }

    private Set<Symbol> getRHSWithNt(Map<Symbol, Set<Symbol>> firsts, Rule r, List<Symbol> rhs) {
        Set<Symbol> firstsRule = new HashSet<>();
        Symbol firstSymbol;
        if (rhs.isEmpty()) {
            firstSymbol = Symbol.EMPTY;
        } else {
            firstSymbol = rhs.get(0);
        }
        //add the symbol to the set of firsts for this lhs
        firstsRule.add(firstSymbol);

        //add the first set of the rule to the list for the lhs
        if (firsts.get(r.getLHS()) != null) {
            firstsRule.addAll(firsts.get(r.getLHS()));
        }
        return firstsRule;
    }


    @Override
    public Map<NonTerm, Set<Term>> getFollow() {
        Map<NonTerm, Set<Term>> oldFollow = new HashMap<>();
        Map<NonTerm, Set<Term>> follow = new HashMap<>();
        List<Rule> rules = g.getRules();
        Set<Term> initial = new HashSet<>(Collections.singleton(Symbol.EOF));
        follow.put(rules.get(0).getLHS(), initial);
        Map<Symbol, Set<Term>> firsts = getFirst();
        while (!oldFollow.equals(follow) && !follow.isEmpty()) {
            oldFollow.putAll(follow);
            for (Rule r : rules) {
                if (terms.containsAll(r.getRHS())) {
                    //do nothing with this rule
                    continue;
                } else {
                    for (int i = 0; i < r.getRHS().size(); i++) {
                        Symbol s = r.getRHS().get(i);
                        //for each symbol in rhs check if it is a non-terminal
                        if (nonTerms.contains(s)) {

                            Set<Term> oldFollows = new HashSet<>();
                            if (follow.get(s) != null) {
                                oldFollows = new HashSet<>(follow.get(s));
                            }
                            //for each non-term get the current follow
                            List<Symbol> newPotentialFollows = r.getRHS().subList(i + 1, r.getRHS().size());
                            //make a list of the potential new follow symbols
                            for (Symbol t : newPotentialFollows) {
                                if (nonTerms.contains(t)) {
                                    //if the new follow is a non-terminal
                                    if (!firsts.get(t).contains(Symbol.EMPTY)) {
                                        // if it has no empty firsts add the firsts of that non-terminal and stop updating follow
                                        oldFollows.addAll(firsts.get(t));
                                        break;
                                    } else {
                                        //if the non-terminal does have empty firsts add the firsts of that non-terminal and continue to add the potential symbols after it
                                        Set<Term> newFollows = new HashSet<>(firsts.get(t));
                                        newPotentialFollows.remove(Symbol.EMPTY);
                                        oldFollows.addAll(newFollows);
                                    }
                                } else if (terms.contains(t)) {
                                    //if the next symbol is a terminal just add the terminal to the follow and done
                                    oldFollows.add((Term) t);
                                    break;
                                } else if (t.equals(Symbol.EOF)) {
                                    //if the next symbol is the end of file symbol add that to the follow
                                    oldFollows.add(Symbol.EOF);
                                    break;
                                }

                            }
                            if (oldFollows.size() == 0){
                                oldFollows.add(Symbol.EOF);
                            }
                            follow.put((NonTerm) s, oldFollows);
                        }
                    }
                }
            }
        }
        //while follow sets are changing

        return follow;
    }

    @Override
    public Map<Rule, Set<Term>> getFirstp() {
        Map<Rule, Set<Term>> firstPlus = new HashMap<>();
        List<Rule> rules = g.getRules();
        for (Rule r : rules) {
            Set<Term> firstPlusRule = new HashSet<>();
            List<Symbol> rhs = r.getRHS();

            if (rhs.get(0).equals(Symbol.EMPTY)) {
                //if the rhs only contains empty add the follow of the lhs to the first plus
                Map<NonTerm, Set<Term>> follows = getFollow();
                firstPlusRule.addAll(follows.get(r.getLHS()));
            } else {
                //if the rhs does not start with empty
                if(terms.contains(rhs.get(0))) {
                    //if the rhs has a terminal as first symbol this is the first plus of this rule
                    firstPlusRule.add((Term) rhs.get(0));
                }
                else if(nonTerms.contains(rhs.get(0))) {
                    //if the rhs has a non-terminal as first symbol add the firsts of this nt to the first plus
                    Map<Symbol,Set<Term>> firsts = getFirst();
                    Set<Term> ntfirsts = firsts.get(rhs.get(0));
                    if(ntfirsts.contains(Symbol.EMPTY)) {
                        boolean done = false;
                        for (Symbol s : rhs) {
                            ntfirsts = new HashSet<>( firsts.get(s));
                            ntfirsts.remove(Symbol.EMPTY);
                            firstPlusRule.addAll(ntfirsts);
                            //if the firsts of the nt contain an empty add the firsts of the next nt
                            if(!ntfirsts.contains(Symbol.EMPTY)){
                                done = true;
                                break;
                            }
                        }
                        if(!done){
                            //if the firsts of the last nt also contains empty add the follow of the lhs
                            Map<NonTerm,Set<Term>> follows = getFollow();
                            firstPlusRule.addAll(follows.get(rhs.get(0)));
                        }

                    }
                    else{firstPlusRule.addAll(ntfirsts);}

                }
            }
            firstPlus.put(r,firstPlusRule);
        }

        return firstPlus;
    }

    @Override
    public boolean isLL1() {
        Map<Rule, Set<Term>> firstPlus = getFirstp();
        //get first plus sets
        Map<NonTerm, List<Rule>> nonTermRules= new HashMap<>();
        for(NonTerm nt : nonTerms){
            List<Rule> init = new ArrayList<>();
            nonTermRules.put(nt,init);
        }
        //make a list of rules per non-terminal to check with
        for(Rule r : g.getRules()){
            NonTerm nt = r.getLHS();
            List<Rule> ntRules = nonTermRules.get(nt);
            ntRules.add(r);
            nonTermRules.put(nt,ntRules);
        }

        for (NonTerm nt : nonTerms){
            List<Rule> ntRules = nonTermRules.get(nt);
            List<Term> firstPlusses = new ArrayList<>();
            Set<Term> firstPlusFiltered = new HashSet<>();
            Map<Symbol, Set<Symbol>> firsts = new HashMap<>();

            for(Rule r : ntRules){
                //check if not left recursive by checking if any nt leads to itself
                List<Symbol> rhs = r.getRHS();
                Set<Symbol> firstsRule = getRHSWithNt(firsts, r, rhs);
                if(firstsRule.contains(nt)){return false;}

                //check if there are any doubles in the first plus sets of the non term rules
                firstPlusses.addAll(firstPlus.get(r));
                firstPlusFiltered.addAll(firstPlus.get(r));
            }
            if(firstPlusses.size() > firstPlusFiltered.size()){
                return false;
            }

        }

        return true;
    }
}
