/**
 * 
 */
package pp.block2.cc.ll;

import pp.block2.cc.NonTerm;
import pp.block2.cc.SymbolFactory;
import pp.block2.cc.Term;
import pp.block2.cc.ll.If;
import pp.block2.cc.ll.Sentence;
import pp.block2.cc.ll.CC2;

/**
 * Class containing some example grammars.
 * @author Arend Rensink
 *
 */
public class Grammars {
	/** Returns a grammar for simple English sentences. */
	public static Grammar makeSentence() {
		// Define the non-terminals
		NonTerm sent = new NonTerm("Sentence");
		NonTerm subj = new NonTerm("Subject");
		NonTerm obj = new NonTerm("Object");
		NonTerm mod = new NonTerm("Modifier");
		// Define the terminals, using the Sentence.g4 lexer grammar
		// Make sure you take the token constantss from the right class!
		SymbolFactory fact = new SymbolFactory(Sentence.class);
		Term noun = fact.getTerminal(Sentence.NOUN);
		Term verb = fact.getTerminal(Sentence.VERB);
		Term adj = fact.getTerminal(Sentence.ADJECTIVE);
		Term end = fact.getTerminal(Sentence.ENDMARK);
		// Build the context free grammar
		Grammar g = new Grammar(sent);
		g.addRule(sent, subj, verb, obj, end);
		g.addRule(subj, noun);
		g.addRule(subj, mod, subj);
		g.addRule(obj, noun);
		g.addRule(obj, mod, obj);
		g.addRule(mod, adj);
		return g;
	}

	public static Grammar makeIf(){
		NonTerm stat = new NonTerm("Statement");
		NonTerm elsePart = new NonTerm("ElsePart");
		// Define the terminals, using the Sentence.g4 lexer grammar
		// Make sure you take the token constantss from the right class!
		SymbolFactory fact = new SymbolFactory(If.class);
		Term ifs = fact.getTerminal(If.IF);
		Term cond = fact.getTerminal(If.COND);
		Term thens = fact.getTerminal(If.THEN);
		Term elses = fact.getTerminal(If.ELSE);
		Term assign = fact.getTerminal(If.ASSIGN);
		// Build the context free grammar
		Grammar g = new Grammar(stat);
		g.addRule(stat, assign);
		g.addRule(stat, ifs, cond, thens, stat, elsePart);
		g.addRule(elsePart,elses, stat);

		return g;
	}

	public static Grammar makeCC2(){
		NonTerm L = new NonTerm("L");
		NonTerm Q = new NonTerm("Q");
		NonTerm B = new NonTerm("B");
		NonTerm R = new NonTerm("R");
		NonTerm P = new NonTerm("P");

		SymbolFactory fact = new SymbolFactory(CC2.class);
		Term a = fact.getTerminal(CC2.AS);
		Term b = fact.getTerminal(CC2.BS);
		Term c = fact.getTerminal(CC2.CS);

		Grammar g = new Grammar(L);
		g.addRule(L,R, a);
		g.addRule(L,Q,b,a);
		g.addRule(Q,B,c);
		g.addRule(B,b,B);
		g.addRule(R,a,b,a,P);
		g.addRule(R,c,a,b,a,P);
		g.addRule(P,b,c);
		g.addRule(P,R,P);

		return g;
	}

}
