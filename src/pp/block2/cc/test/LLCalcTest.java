package pp.block2.cc.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import pp.block2.cc.NonTerm;
import pp.block2.cc.Symbol;
import pp.block2.cc.Term;
import pp.block2.cc.ll.*;
import pp.block2.cc.ll.If;
import pp.block2.cc.ll.Sentence;

public class LLCalcTest {
    //sentence grammar tests
    Grammar sentenceG = Grammars.makeSentence();
    // Define the non-terminals
    NonTerm subj = sentenceG.getNonterminal("Subject");
    NonTerm obj = sentenceG.getNonterminal("Object");
    NonTerm sent = sentenceG.getNonterminal("Sentence");
    NonTerm mod = sentenceG.getNonterminal("Modifier");
    // Define the terminals
    Term adj = sentenceG.getTerminal(Sentence.ADJECTIVE);
    Term noun = sentenceG.getTerminal(Sentence.NOUN);
    Term verb = sentenceG.getTerminal(Sentence.VERB);
    Term end = sentenceG.getTerminal(Sentence.ENDMARK);
    // Now add the last rule, causing the grammar to fail
    Grammar sentenceXG = Grammars.makeSentence();

    {
        sentenceXG.addRule(mod, mod, mod);
    }

    LLCalc sentenceXLL = createCalc(sentenceXG);

    /**
     * Tests the LL-calculator for the Sentence grammar.
     */
    @Test
    public void testSentenceOrigLL1() {
        // Without the last (recursive) rule, the grammar is LL-1
        assertTrue(createCalc(sentenceG).isLL1());
    }

    @Test
    public void testSentenceXFirst() {
        Map<Symbol, Set<Term>> first = sentenceXLL.getFirst();
        assertEquals(set(adj, noun), first.get(sent));
        assertEquals(set(adj, noun), first.get(subj));
        assertEquals(set(adj, noun), first.get(obj));
        assertEquals(set(adj), first.get(mod));
    }

    @Test
    public void testSentenceXFollow() {
        // FOLLOW sets
        Map<NonTerm, Set<Term>> follow = sentenceXLL.getFollow();
        assertEquals(set(Symbol.EOF), follow.get(sent));
        assertEquals(set(verb), follow.get(subj));
        assertEquals(set(end), follow.get(obj));
        assertEquals(set(noun, adj), follow.get(mod));
    }

    @Test
    public void testSentenceXFirstPlus() {
        // Test per rule
        Map<Rule, Set<Term>> firstp = sentenceXLL.getFirstp();
        List<Rule> subjRules = sentenceXG.getRules(subj);
        assertEquals(set(noun), firstp.get(subjRules.get(0)));
        assertEquals(set(adj), firstp.get(subjRules.get(1)));
    }

    @Test
    public void testSentenceXLL1() {
        assertFalse(sentenceXLL.isLL1());
    }

    //if grammar tests
    Grammar ifG = Grammars.makeIf (); // to be defined (Ex. 2-CC.4.1)
    // Define the non-terminals
    NonTerm stat = ifG. getNonterminal ("Statement");
    NonTerm elsePart = ifG. getNonterminal ("ElsePart");
    // Define the terminals (take from the right lexer grammar!)
    Term ifT = ifG. getTerminal(If.IF);
    Term cond = ifG.getTerminal(If.COND);
    Term thenT = ifG.getTerminal(If.THEN);
    Term elseT = ifG.getTerminal(If.ELSE);
    Term assign = ifG.getTerminal(If.ASSIGN);

    Term eof = Symbol.EOF;
    Term empty = Symbol.EMPTY;
    LLCalc ifLL = createCalc(ifG );
    @Test
    public void testIfFirst () {
        Map <Symbol , Set <Term >> first = ifLL.getFirst ();
        assertEquals (set(assign, ifT), first.get(stat ));
        assertEquals(set(elseT),first.get(elsePart));

    }
    @Test
    public void testIfFollow () {
        Map <NonTerm , Set <Term >> follow = ifLL.getFollow ();
        assertEquals (set(elseT,Symbol.EOF), follow.get(stat ));
        assertEquals (set(Symbol.EOF), follow.get(elsePart));

    }
    @Test
    public void testIfFirstPlus () {
        Map <Rule , Set <Term >> firstp = ifLL.getFirstp ();
        List<Rule> statRules = ifG.getRules(stat);
        List <Rule > elseRules = ifG.getRules(elsePart );
        assertEquals(set(assign),firstp.get(statRules.get(0)));
        assertEquals(set(ifT),firstp.get(statRules.get(1)));
        assertEquals (set(elseT), firstp.get(elseRules.get (0)));

    }
    @Test
    public void testIfLL1 () {
        assertTrue (ifLL.isLL1 ());
    }

    //CC2 grammar tests
    Grammar CC2G = Grammars.makeCC2 (); // to be defined (Ex. 2-CC.4.1)
    // Define the non-terminals
    NonTerm L = CC2G. getNonterminal ("L");
    NonTerm Q = CC2G. getNonterminal ("Q");
    NonTerm B = CC2G. getNonterminal ("B");
    NonTerm R = CC2G. getNonterminal ("R");
    NonTerm P = CC2G. getNonterminal ("P");

    // Define the terminals (take from the right lexer grammar!)
    Term a = CC2G. getTerminal(CC2.AS);
    Term b = CC2G. getTerminal(CC2.BS);
    Term c = CC2G. getTerminal(CC2.CS);

    LLCalc CC2LL = createCalc(CC2G );
    @Test
    public void testCC2First () {
        Map <Symbol , Set <Term >> first = CC2LL.getFirst ();
        assertEquals (set(a,b,c), first.get(L ));
        assertEquals(set(b),first.get(B));
        assertEquals(set(a,c),first.get(R));
        assertEquals(set(a,b,c),first.get(P));
        assertEquals(set(b),first.get(Q));

    }
    @Test
    public void testCC2Follow () {
        Map <NonTerm , Set <Term >> follow = CC2LL.getFollow ();
        assertEquals (set(Symbol.EOF), follow.get(L ));
        assertEquals(set(b),follow.get(Q));
        assertEquals(set(c),follow.get(B));
        assertEquals(set(a,b,c),follow.get(R));
        assertEquals(set(Symbol.EOF),follow.get(P));
    }
    @Test
    public void testCC2FirstPlus () {
        Map <Rule , Set <Term >> firstp = CC2LL.getFirstp ();
        List <Rule > LRules = CC2G.getRules(L );
        List <Rule > QRules = CC2G.getRules(Q );
        List <Rule > BRules = CC2G.getRules(B );
        List <Rule > RRules = CC2G.getRules(R );
        List <Rule > PRules = CC2G.getRules(P );
        assertEquals (set(a,c), firstp.get(LRules.get (0)));
        assertEquals (set(b), firstp.get(LRules.get (1)));
        assertEquals (set(b), firstp.get(QRules.get (0)));
        assertEquals (set(b), firstp.get(BRules.get (0)));
        assertEquals (set(a), firstp.get(RRules.get (0)));
        assertEquals (set(c), firstp.get(RRules.get (1)));
        assertEquals (set(b), firstp.get(PRules.get (0)));
        assertEquals (set(a,c), firstp.get(PRules.get (1)));

    }
    @Test
    public void testCC2LL1 () {
        assertTrue (CC2LL.isLL1 ());
    }
    /**
     * Creates an LL1-calculator for a given grammar.
     */
    private LLCalc createCalc(Grammar g) {
        return new CC3MyLLCalc(g);
    }

    @SuppressWarnings("unchecked")
    private <T> Set<T> set(T... elements) {
        return new HashSet<>(Arrays.asList(elements));
    }
}
