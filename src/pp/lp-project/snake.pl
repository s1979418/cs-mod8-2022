:- use_module(library(clpfd)). % Import the module
:- set_prolog_flag(clpfd_monotonic, true). % setting to get useful errors sometimes
:- [tests].

%The solver of a snake puzzle
snake(RowClues, ColClues, Grid, Solution):-   copyGrid(Grid,Solution)
                                            , checkRowClues(Solution,RowClues)
                                            , checkColClues(Solution,ColClues)
                                            , check_nums(Solution,2) %grid may only contain 0's,2's and 2 1's
                                            , extend_grid(Solution, Solution2)
                                            , nonTouching(Solution2) % snake cannot touch itself
                                            , countNeighbors(Solution2) % heads have 1 neighbor, midpoints 2
                                            %, snakeConnected(Solution2) % snake must be connected
                                            .

%-------------------------------------------------------------------------|copyGrid/2|-------------------------------------------------------------------------

    %copies grid except for -1 values
    copyGrid([],[]).
    copyGrid([Row|G],[RowS|S]) :-   copyRow(Row,RowS), copyGrid(G,S).

    %copies a row except for -1 values
    copyRow([],[]).
    copyRow([-1|R],[_|S]) :-        copyRow(R,S).
    copyRow([Clue|R],[Clue|S]) :-   copyRow(R,S).

%-------------------------------------------------------------------------|checkRowClues/2|-------------------------------------------------------------------------

    %checks whether the rows of the solution match the clues
    checkRowClues([],[]).
    %since -1 gives no hint the sum of row S does not matter
    checkRowClues([_|XS],[-1|RS]):- checkRowClues(XS,RS).
    checkRowClues([S|XS],[R|RS]):- sumlist(S,R),checkRowClues(XS,RS).

    %sums the values of a list
    sumlist([],0).
    %if the row head is 0 ignore and continue summing the rest of the row
    sumlist([0|XS],S):- sumlist(XS,S).
    %bodyparts, heads and tails all count as 1
    sumlist([1|XS],S):- sumlist(XS,Sd), S is Sd +1.
    sumlist([2|XS],S):- sumlist(XS,Sd), S is Sd +1.

%-------------------------------------------------------------------------|checkColClues/2|-------------------------------------------------------------------------

    %transposes the grid so the columns become rows, then runs chechRowClues
    checkColClues(Solution,ColClues):- transpose(Solution,Strans), checkRowClues(Strans,ColClues).

%-------------------------------------------------------------------------|check_nums/2|-------------------------------------------------------------------------

    %counts number of 1's in grid and checks that there are no numbers other than 0, 1, and 2.
    %checks row by row if numbers are valid
    check_nums([],0).
    check_nums([Row|Rows],P):- check_nums_row(Row,M),check_nums(Rows,N), P is M+N.
    %checks if row is valid and counts number of 1's in row
    check_nums_row([],0).
    check_nums_row([0|CS],N):- check_nums_row(CS,N).
    check_nums_row([1|CS],1):- check_nums_row(CS,0).
    check_nums_row([1|CS],2):- check_nums_row(CS,1).
    check_nums_row([2|CS],N):- check_nums_row(CS,N).
    %fails if number is not 0, 1, or 2
    check_nums_row([_|_],_):- fail.

%-------------------------------------------------------------------------|Extend_grid/2|-------------------------------------------------------------------------

    %extend grid size on copy
    extend_grid(OldGrid,NewGrid) :- transpose(OldGrid,TransGrid),
                                    extend_grid_rows(TransGrid,RowTransGrid),
                                    transpose(RowTransGrid,RowGrid),
                                    extend_grid_rows(RowGrid,NewGrid).

    %extends all rows by adding a 0 at both ends
    extend_grid_rows([Row],[NewGrid]) :- extend_row(Row,NewGrid).
    extend_grid_rows([Row|Rows],NewGrid) :- extend_row(Row,NewRow), extend_grid_rows(Rows,NewRows), NewGrid = [NewRow|NewRows].

    % Extend a row by adding a 0 at both ends
    extend_row(OldRow,NewRow) :- append([0|OldRow],[0],NewRow).

%-------------------------------------------------------------------------|nonTouching/1|-------------------------------------------------------------------------

    %checks that no illigal diagonal connections exist in the snake
    nonTouching([_]). %The last row of the solution has no connections to the 0-row.
    nonTouching([R1,R2|Rest]) :- nonTouchingRows(R1,R2),
                                nonTouching([R2|Rest]).

    %This checks of a piece has downward diagonal connections. If they have upward connections, they are checked by the previous iteration.
    nonTouchingRows([W,Piece,E],[SW,S,SE]) :-   Piece = 0;										%We're looking at a non-piece, no further investigation required.
                                                1 #=< Piece, SW=0, SE=0;                        %No diagonal connections, this is fine.
                                                1 #=< Piece, SW #>= 1, SE=0,(W=2;S=2);          %Bottom-left connection is allowed if there is a body piece to the left or bottom.
                                                1 #=< Piece, SE #>= 1, SW=0,(S=2;E=2);          %Bottom-right diagonal connection is allowed if there is a body piece to the right ot bottom.
                                                1 #=< Piece, SW #>= 1, SE #>= 1,W=2,E=2.        %Connections to both bottom-left and right are allowed if left and right have snake body pieces.
    nonTouchingRows([W,Piece,E|R1],[SW,S,SE|R2]) :- Piece = 0,									%We're looking at a non-piece, no further investigation required.
                                                    nonTouchingRows([Piece,E|R1],[S,SE|R2]);
                                                    1 #=< Piece, SW=0, SE=0,                    %No diagonal connections, this is fine.
                                                    nonTouchingRows([Piece,E|R1],[S,SE|R2]);
                                                    1 #=< Piece, SW #>= 1, SE=0,(W=2;S=2),      %Bottom-left connection is allowed if there is a body piece to the left or bottom.
                                                    nonTouchingRows([Piece,E|R1],[S,SE|R2]);
                                                    1 #=< Piece, SE #>=1, SW=0,(S=2;E=2),       %Bottom-right diagonal connection is allowed if there is a body piece to the right ot bottom.
                                                    nonTouchingRows([Piece,E|R1],[S,SE|R2]);
                                                    1 #=< Piece, SW #>= 1, SE #>= 1,W=2,E=2,    %Connections to both bottom-left and right are allowed if left and right have snake body pieces.
                                                    nonTouchingRows([Piece,E|R1],[S,SE|R2]).

%-------------------------------------------------------------------------|countNeighbors/1|-------------------------------------------------------------------------

    %iterates all rows of the solution and passes them to check_neighbors_rows to count neighbour counts
    countNeighbors([Row1,Row2,Row3]) :- check_neighbors_rows(Row1,Row2,Row3).
    countNeighbors([Row1,Row2,Row3|Rest]) :- check_neighbors_rows(Row1,Row2,Row3), countNeighbors([Row2,Row3|Rest]).

    %checks 3 rows at a time for correct neighbour counts by passing them to check_neighbors_pattern/5
    check_neighbors_rows([_,N,_],[W,M,E],[_,S,_]) :- check_neighbors_pattern(M,N,E,S,W).
    check_neighbors_rows([_,N,A3|RowA],[W,M,E|RowB],[_,S,C3|RowC]) :-   check_neighbors_pattern(M,N,E,S,W),
                                                                        check_neighbors_rows([N,A3|RowA],[M,E|RowB],[S,C3|RowC]).

    %Neighbours are N E S W, tests if a piece is non-zero , then if piece is correct
    % Piece = 0 -> empty, continue
    check_neighbors_pattern(0,_,_,_,_).
    % Piece = 1 -> head, has 1 neighbour so sum of count_cell is 1 == Piece
    % Piece = 2 -> body, has 2 neighbours so sum of count_cell is 2 == Piece
    check_neighbors_pattern(Piece,N,E,S,W) :-   1 #=< Piece,
                                                count_cell(N,X1),
                                                count_cell(E,X2),
                                                count_cell(S,X3),
                                                count_cell(W,X4),
                                                Piece #= X1+X2+X3+X4.

    %count_cell/2 counts zero entries as 0 and others as 1
    count_cell(0,0).
    count_cell(C,1):- C =1 ; C=2.
    count_cell(_,_):- fail. %for the unlikely case we get a faulty puzzle

%-------------------------------------------------------------------------|snakeConnected/1|-------------------------------------------------------------------------

    %steps to check if snake connected:
    %base cases
    snakeConnected([_,_]). %2x2 grid snake is always connected
    snakeConnected([[_,_,_],[_,_,_],[_,_,_]]). %3x3 grid snake is always connected
    %main check, 'colour' connected pieces, then check if there are any uncoloured pieces
    snakeConnected(Solution):- connectColour(Solution,0,0,Connected), check4s(Connected).

%-------------------------------------------------------------|check4s/1|------------------------------------------------------------------------------------------------
    %checks that the grid only contains 4's and 0's
    %base case
    check4s([]).
    %checks all rows one by one
    check4s([Row|Rows]):- check4rows(Row), check4s(Rows).

    %checks that the row only contains 4's and 0's
    %base case
    check4rows([]).
    %if the row head is 0 or 4, ignore and continue checking the rest of the row
    check4rows([0|XS]):- check4rows(XS).
    check4rows([4|XS]):- check4rows(XS).
    %if the row head is 1 or 2 the grid does not just contain 4's and 0's
    check4rows([1|_]):- fail.
    check4rows([2|_]):- fail.

%-------------------------------------------------------------------------|connectColour/4|-------------------------------------------------------------------------
    %colours all connected body parts, making them 4
    %base case to start colouring
    connectColour(Solution,0,0,Connected):-
        findHead(Solution,C,R,ColHead),
        connectColour(ColHead,C,R,Connected).

    %case when tail is found, done
    connectColour(Solution,C,R,Connected):-
        goToLocation(Solution,C,R,Loc),
        colour(Loc,Coloured,0,0),
        replaceGrid(Coloured,Solution,C,R, Connected).

    %case for during colouring
    connectColour(Solution,C,R,Connected):-
        goToLocation(Solution,C,R,Loc),
        colour(Loc,Coloured,Cc,Rc),
        replaceGrid(Coloured,Solution,C,R, ColSol),
        Cs is C + Cc, Rs is R + Rc,
        connectColour(ColSol,Cs,Rs,Connected).

%-------------------------------------------------------------------------|goToLocation/4|-------------------------------------------------------------------------
    %goes to a certain column and row and returns 3 in each direction (Nw,N,Ne,W,M,E,Sw,S,Se)
    goToLocation([[Nw,N,Ne|_],[W,M,E|_],[Sw,S,Se|_]|_],0,0,[[Nw,N,Ne],[W,M,E],[Sw,S,Se]]).
    goToLocation([[_|Row1],[_|Row2],[_|Row3]|Rows],C,0,Loc):- Cs is C-1, goToLocation([Row1,Row2,Row3|Rows],Cs,0,Loc).
    goToLocation([_|Rows],C,R,Loc):- Rs is R-1, goToLocation(Rows,C,Rs,Loc).
    
    
%-------------------------------------------------------------------------|colour/4|-------------------------------------------------------------------------
    %colours the connected body pieces and returns the coloured rows as well as the direction it needs to go next
    %around 0 colouring adjustment
    colour([[4,2,Ne],[W,0,E],[Sw,S,Se]],[[4,2,Ne],[W,0,E],[Sw,S,Se]],0,-1).     %head left top in north row
    colour([[Nw,4,2],[W,0,E],[Sw,S,Se]],[[Nw,4,2],[W,0,E],[Sw,S,Se]],1,-1).     %go east in north row
    colour([[2,4,Ne],[W,0,E],[Sw,S,Se]],[[2,4,Ne],[W,0,E],[Sw,S,Se]],-1,-1).    %go west in north row

    colour([[Nw,N,Ne],[W,0,E],[Sw,4,2]],[[Nw,N,Ne],[W,0,E],[Sw,4,2]],1,1).      %go east in south row
    colour([[Nw,N,Ne],[W,0,E],[2,4,Se]],[[Nw,N,Ne],[W,0,E],[2,4,Se]],-1,1).    %go west in south row

    colour([[2,N,Ne],[4,0,E],[Sw,S,Se]],[[2,N,Ne],[4,0,E],[Sw,S,Se]],-1,-1).    %go north in west row
    colour([[Nw,N,Ne],[4,0,E],[2,S,Se]],[[Nw,N,Ne],[4,0,E],[2,S,Se]],-1,1).     %go south in west row

    colour([[Nw,N,2],[W,0,4],[Sw,S,Se]],[[Nw,N,2],[W,0,4],[Sw,S,Se]],1,-1).     %go north in east row
    colour([[Nw,N,Ne],[W,0,4],[Sw,S,2]],[[Nw,N,Ne],[W,0,4],[Sw,S,2]],1,1).      %go south in east row

    %body colouring
    colour([[Nw,4,Ne],[W,2,E],[Sw,2,Se]],[[Nw,4,Ne],[W,4,E],[Sw,2,Se]],0,1).    %go south from north
    colour([[Nw,4,Ne],[W,2,2],[Sw,S,Se]],[[Nw,4,Ne],[W,4,2],[Sw,S,Se]],1,0).    %go east from north
    colour([[Nw,4,Ne],[2,2,E],[Sw,S,Se]],[[Nw,4,Ne],[2,4,E],[Sw,S,Se]],-1,0).   %go west from north

    colour([[Nw,2,Ne],[W,2,E],[Sw,4,Se]],[[Nw,2,Ne],[W,4,E],[Sw,4,Se]],0,-1).   %go north from south
    colour([[Nw,N,Ne],[W,2,2],[Sw,4,Se]],[[Nw,N,Ne],[W,4,2],[Sw,4,Se]],1,0).    %go east from south
    colour([[Nw,N,Ne],[2,2,E],[Sw,4,Se]],[[Nw,N,Ne],[2,4,E],[Sw,4,Se]],-1,0).   %go west from south

    colour([[Nw,N,Ne],[4,2,2],[Sw,S,Se]],[[Nw,N,Ne],[4,4,2],[Sw,S,Se]],1,0).    %go east from west
    colour([[Nw,2,Ne],[4,2,E],[Sw,S,Se]],[[Nw,2,Ne],[4,4,E],[Sw,S,Se]],0,-1).   %go north from west
    colour([[Nw,N,Ne],[4,2,E],[Sw,2,Se]],[[Nw,N,Ne],[4,4,E],[Sw,2,Se]],0,1).    %go south from west

    colour([[Nw,N,Ne],[2,2,4],[Sw,S,Se]],[[Nw,N,Ne],[2,4,4],[Sw,S,Se]],-1,0).   %go west from east
    colour([[Nw,2,Ne],[W,2,4],[Sw,S,Se]],[[Nw,2,Ne],[W,4,4],[Sw,S,Se]],0,-1).   %go north from east
    colour([[Nw,N,Ne],[W,2,4],[Sw,2,Se]],[[Nw,N,Ne],[W,4,4],[Sw,2,Se]],0,1).    %go south from east

     %tail colouring
    colour([[Nw,4,Ne],[W,1,E],[Sw,S,Se]],[[Nw,4,Ne],[W,4,E],[Sw,S,Se]],0,0).    %from north

    colour([[Nw,N,Ne],[W,1,E],[Sw,4,Se]],[[Nw,N,Ne],[W,4,E],[Sw,4,Se]],0,0).    %from south

    colour([[Nw,N,Ne],[4,1,E],[Sw,S,Se]],[[Nw,N,Ne],[4,4,E],[Sw,S,Se]],0,0).    %from west

    colour([[Nw,N,Ne],[W,1,4],[Sw,S,Se]],[[Nw,N,Ne],[W,4,4],[Sw,S,Se]],0,0).    %from east

%-------------------------------------------------------------------------|findHead/4|-------------------------------------------------------------------------
    %finds and colours the head with 4
    %base cases
    
    %head found, colour it with 4
    findHead([[Nw,N,Ne|Row1],[W,1,E|Row2],[Sw,S,Se|Row3]|Rows],0,0,[[Nw,N,Ne|Row1],[W,4,E|Row2],[Sw,S,Se|Row3]|Rows]).
    %head not in this grid
    findHead([],_,_,_):-fail.
    %head not in this row
    findHead([[]|_],_,_,_):-fail.

    %look for head in this row & column, if not found move to the next row & column
    findHead([[Nw,N,Ne|Row1],[W,M,E|Row2],[Sw,S,Se|Row3]|Rows],C,R,ColHead):- 
        (findHead([[N,Ne|Row1],[M,E|Row2],[S,Se|Row3]|Rows],Cs,R,[[Nh,Neh|Row1h],[Mh,Eh|Row2h],[Sh,Seh|Row3h]|Rowsh]), C is Cs + 1,
        ColHead = [[Nw,Nh,Neh|Row1h],[W,Mh,Eh|Row2h],[Sw,Sh,Seh|Row3h]|Rowsh]);

        (findHead([[W,M,E|Row2],[Sw,S,Se|Row3]|Rows],C,Rs,[[Wh,Mh,Eh|Row2h],[Swh,Sh,Seh|Row3h]|Rowsh]), R is Rs + 1,
        ColHead = [[Nw,N,Ne|Row1],[Wh,Mh,Eh|Row2h],[Swh,Sh,Seh|Row3h]|Rowsh]);

        (findHead([[M,E|Row2],[S,Se|Row3]|Rows],Cs,Rs,[[Mh,Eh|Row2h],[Sh,Seh|Row3h]|Rowsh]), R is Rs + 1, C is Cs + 1,
        ColHead = [[Nw,N,Ne|Row1],[W,Mh,Eh|Row2h],[Sw,Sh,Seh|Row3h]|Rowsh]).




%-------------------------------------------------------------------------|replaceGrid/5|-------------------------------------------------------------------------
    %replaces part of the grid with a given 3 rows in a given location
    % model: replaceGrid(ColouredRows,Grid,Column,Row,ReplacedGrid)
    %replace the part at the given location
    replaceGrid([[Nwc,Nc,Nec],[Wc,Mc,Ec],[Swc,Sc,Sec]],[[_,_,_|Row1],[_,_,_|Row2],[_,_,_|Row3]|Rows],0,0,[[Nwc,Nc,Nec|Row1],[Wc,Mc,Ec|Row2],[Swc,Sc,Sec|Row3]|Rows]).

    %go to given location to replace part, then build the grid back up
    replaceGrid(Coloured,[[Nw,N,Ne|Row1],[W,M,E|Row2],[Sw,S,Se|Row3]|Rows],C,0,ReplacedGrid):-
        Cs is C - 1,
        replaceGrid(Coloured , [[N,Ne|Row1],[M,E|Row2],[S,Se|Row3]|Rows] , Cs , 0 , [[Nc,Nec|Row1c],[Mc,Ec|Row2c],[Sc,Sec|Row3c]|Rowsc]),
        ReplacedGrid = [[Nw,Nc,Nec|Row1c],[W,Mc,Ec|Row2c],[Sw,Sc,Sec|Row3c]|Rowsc].

    replaceGrid(Coloured,[[Nw,N,Ne|Row1],[W,M,E|Row2],[Sw,S,Se|Row3]|Rows],C,R,ReplacedGrid):-
        Rs is R - 1,
        replaceGrid(Coloured,[[W,M,E|Row2],[Sw,S,Se|Row3]|Rows],C,Rs,[[Wc,Mc,Ec|Row2c],[Swc,Sc,Sec|Row3c]|Rowsc]),
        ReplacedGrid = [[Nw,N,Ne|Row1],[Wc,Mc,Ec|Row2c],[Swc,Sc,Sec|Row3c]|Rowsc].

