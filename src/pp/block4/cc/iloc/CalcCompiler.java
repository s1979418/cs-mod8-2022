package pp.block4.cc.iloc;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import pp.block4.cc.ErrorListener;
import pp.iloc.Simulator;
import pp.iloc.model.*;

/**
 * Compiler from Calc.g4 to ILOC.
 */
public class CalcCompiler extends CalcBaseListener {
    /**
     * Program under construction.
     */
    private Program prog;
    // Attribute maps and other fields
    private int nextreg = 0;

    private ParseTreeProperty<Reg> registers;

    /**
     * Compiles a given expression string into an ILOC program.
     */
    public Program compile(String text) {
        Program result = null;
        ErrorListener listener = new ErrorListener();
        CharStream chars = CharStreams.fromString(text);
        Lexer lexer = new CalcLexer(chars);
        lexer.removeErrorListeners();
        lexer.addErrorListener(listener);
        TokenStream tokens = new CommonTokenStream(lexer);
        CalcParser parser = new CalcParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(listener);
        ParseTree tree = parser.complete();
        if (listener.hasErrors()) {
            System.out.printf("Parse errors in %s:%n", text);
            for (String error : listener.getErrors()) {
                System.err.println(error);
            }
        } else {
            result = compile(tree);
        }
        return result;
    }

    /**
     * Compiles a given Calc-parse tree into an ILOC program.
     */
    public Program compile(ParseTree tree) {
        this.nextreg = 0;
        this.prog = new Program();
        this.registers = new ParseTreeProperty<>();
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(this, tree);
        return this.prog;
    }

    @Override
    public void exitComplete(CalcParser.CompleteContext ctx) {
        //result will be in last register
//		"out \"Outcome: \",r_"+(nextreg-1)
        emit(OpCode.out, new Str("Outcome: "), registers.get(ctx.expr()));
    }

    @Override
    public void exitTimes(CalcParser.TimesContext ctx) {
//		"mult r_"+(nextreg -2)+" , r_"+(nextreg-1)+" => r_"+nextreg + "\n";
        Reg reg = new Reg("r_" + nextreg);
        emit(OpCode.mult, registers.get(ctx.expr(0)), registers.get(ctx.expr(1)), reg);
        registers.put(ctx, reg);
        nextreg++;
    }

    @Override
    public void exitMinus(CalcParser.MinusContext ctx) {
//		"sub I_"+(nextreg -1)+" , 0 => r_"+nextreg + "\n";
        Reg reg = new Reg("r_" + nextreg);
        emit(OpCode.multI, registers.get(ctx.expr()), new Num(-1), reg);
        registers.put(ctx, reg);
        nextreg++;
    }

    @Override
    public void exitPlus(CalcParser.PlusContext ctx) {
//		"add r_"+(nextreg -2)+" , r_"+(nextreg-1)+" => r_"+nextreg + "\n";
        Reg reg = new Reg("r_" + nextreg);
        emit(OpCode.add, registers.get(ctx.expr(0)), registers.get(ctx.expr(1)), reg);
        registers.put(ctx, reg);
        nextreg++;
    }

    @Override
    public void exitPar(CalcParser.ParContext ctx) {
        //make sure next node up has access to the result register of the expression in parentheses
        registers.put(ctx, registers.get(ctx.expr()));
    }

    @Override
    public void exitNumber(CalcParser.NumberContext ctx) {
        int num = Integer.parseInt(ctx.NUMBER().getSymbol().getText());
        Reg reg = new Reg("r_" + nextreg);
//		"loadI " + num + " => r_" + nextreg + "\n";
        emit(OpCode.loadI, new Num(num), reg);
        registers.put(ctx, reg);
        nextreg++;
    }


    /**
     * Constructs an operation from the parameters
     * and adds it to the program under construction.
     */
    private void emit(OpCode opCode, Operand... args) {
        this.prog.addInstr(new Op(opCode, args));
    }

    /**
     * Calls the compiler, and simulates and prints the compiled program.
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("Usage: [expr]+");
            return;
        }
        CalcCompiler compiler = new CalcCompiler();
        for (String expr : args) {
            System.out.println("Processing " + expr);
            Program prog = compiler.compile(expr);
            new Simulator(prog).run();
            System.out.println(prog.prettyPrint());
        }
    }


}
