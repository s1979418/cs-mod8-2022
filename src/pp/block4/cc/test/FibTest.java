package pp.block4.cc.test;

import org.junit.Assert;
import org.junit.Test;
import pp.iloc.Assembler;
import pp.iloc.Simulator;
import pp.iloc.eval.Machine;
import pp.iloc.model.Program;
import pp.iloc.parse.FormatException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.fail;

public class FibTest {


    @Test
    public void testMultiple(){
        //fib as used in the program:
        int[] fib = new int[]{1,1,2,3,5,8,13,21,34};
        for(int n=0; n<fib.length; n++) {
            int expected = fib[n];
            programTester(n,expected);
        }
    }
    @Test
    public void findMaxN(){
        for(int i =0; i<50; i++){
                String res = programTester(i);
                if(res.contains("-")){
                    System.out.println("Integer overflow at n = "+i+" with result "+res);
                    System.out.println("Last valid result at n = "+(i-1)+" with result "+programTester(i-1));
                    break;
                }
        }
    }

    public void programTester(int n, int expected){
        File fileR = new File("src/pp/block4/cc/iloc/fibRtoR.iloc");
        String zR = "";
        try {
            Program result = Assembler.instance().assemble(fileR);
            Simulator simulator = new Simulator(result);
            ByteArrayOutputStream outR = new ByteArrayOutputStream();
            simulator.setOut(outR);
            Machine vm = simulator.getVM();
            vm.clear();
            vm.init("n",n);
            vm.setNum("z",2);
            simulator.run();
            zR = outR.toString();

        } catch (FormatException | IOException e) {
            fail(e.getMessage());
        }

        File fileM = new File("src/pp/block4/cc/iloc/fibMtoM.iloc");
        String zM = "0";

        try {
            Program resultM = Assembler.instance().assemble(fileM);
            Simulator simulatorM = new Simulator(resultM);
            Machine vmM = simulatorM.getVM();
            ByteArrayOutputStream outM = new ByteArrayOutputStream();
            simulatorM.setOut(outM);
            vmM.init("n",n);
            vmM.init("x",0);
            vmM.init("y",0);
            vmM.init("z",0);
            simulatorM.run();
            outM.close();
            zM = outM.toString();

        } catch (FormatException | IOException e) {
            fail(e.getMessage());
        }
        Assert.assertEquals(zM,zR);
        String comparer = expected + "\n";
        Assert.assertEquals(comparer,zM);

    }

    public String programTester(int n){
        File fileR = new File("src/pp/block4/cc/iloc/fibRtoR.iloc");
        String zR = "";
        try {
            Program result = Assembler.instance().assemble(fileR);
            Simulator simulator = new Simulator(result);
            ByteArrayOutputStream outR = new ByteArrayOutputStream();
            simulator.setOut(outR);
            Machine vm = simulator.getVM();
            vm.clear();
            vm.init("n",n);
            vm.setNum("z",2);
            simulator.run();
            zR = outR.toString();

        } catch (FormatException | IOException e) {
            fail(e.getMessage());
        }

        File fileM = new File("src/pp/block4/cc/iloc/fibMtoM.iloc");
        String zM = "0";

        try {
            Program resultM = Assembler.instance().assemble(fileM);
            Simulator simulatorM = new Simulator(resultM);
            Machine vmM = simulatorM.getVM();
            ByteArrayOutputStream outM = new ByteArrayOutputStream();
            simulatorM.setOut(outM);
            vmM.init("n",n);
            vmM.init("x",0);
            vmM.init("y",0);
            vmM.init("z",0);
            simulatorM.run();
            outM.close();
            zM = outM.toString();

        } catch (FormatException | IOException e) {
            fail(e.getMessage());
        }
        Assert.assertEquals(zM,zR);
        return zR;
    }
}
