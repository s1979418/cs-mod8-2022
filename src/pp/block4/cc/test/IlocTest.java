package pp.block4.cc.test;
import org.junit.Test;
import pp.iloc.*;
import pp.iloc.model.Program;
import pp.iloc.parse.FormatException;
import pp.iloc.eval.Machine;

import java.io.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class IlocTest {
    @Test
    public void prettyPrintTest(){
        File file = new File("src/pp/block4/cc/iloc/max.iloc");
        try {
            Program result = Assembler.instance().assemble(file);
            String print = result.prettyPrint();

            Program secondparse = Assembler.instance().assemble(print);
            assertEquals(result, secondparse);

        } catch (FormatException | IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void programtester(){
        File file = new File("src/pp/block4/cc/iloc/max.iloc");
        try {
            Program result = Assembler.instance().assemble(file);
            Simulator simulator = new Simulator(result);
            Machine vm = simulator.getVM();
            vm.setNum("alength", 3);
            vm.init("a", 5, 2, 15);
            simulator.run();

        } catch (FormatException | IOException e) {
            fail(e.getMessage());
        }
    }

}
