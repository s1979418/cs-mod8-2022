package pp.block4.cp.threaddumps;

import net.jcip.annotations.ThreadSafe;
import nl.utwente.pp.cp.junit.ConcurrentRunner;
import nl.utwente.pp.cp.junit.ThreadNumber;
import nl.utwente.pp.cp.junit.Threaded;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@ThreadSafe
@RunWith(ConcurrentRunner.class)
public class LeftRightLockTest {
    public LeftRightDeadlock locker;
    private final int THREADS = 10;

    @Before
    public void before(){
        locker = new LeftRightDeadlock();
    }

    @Test
    public void singleTestLeft(){
        locker.leftRight();
        Assert.assertTrue(true);
    }

    @Test
    @Threaded(count = 10)
    public void multiTest(
            @ThreadNumber int threadNumber)	 {
        this.getLock(threadNumber,this.locker);
    }

    @Test
    public void singleTestRight(){
        locker.rightLeft();
        Assert.assertTrue(true);
    }
    public void getLock(int thread, LeftRightDeadlock locker){
        if(thread%2 ==0){
            locker.leftRight();
        }
        else {locker.rightLeft();}
    }

}
