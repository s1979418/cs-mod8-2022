package pp.block4.cp.locks;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.atomic.AtomicLongArray;

public class MyBasicLock implements BasicLock {
    AtomicIntegerArray enterBooleans;
    volatile int turn;
    volatile int lockCount;

   AtomicLongArray timers;

    volatile long start;
    volatile long finish;
    public MyBasicLock() {
        this.enterBooleans = new AtomicIntegerArray(2);
        this.enterBooleans.set(0,0);
        this.enterBooleans.set(1,0);
        this.timers = new AtomicLongArray(2);
        this.timers.set(0, 0L);
        this.timers.set(1,0L);
        this.turn = 0;
    }

    @Override
    public void lock(int threadNumber) {
        this.enterBooleans.set(threadNumber,1);
        int nextThread = threadNumber;
        for(int i=nextThread;i<this.enterBooleans.length();i++){
            if(this.enterBooleans.get(i)==1){
                nextThread = i;
                break;
            }
        }
        if(nextThread==threadNumber){
                for (int i = 0; i < threadNumber; i++) {
                    if (this.enterBooleans.get(i) == 1) {
                        nextThread = i;
                        break;
                    }
                }
            }

        while(timers.get(threadNumber)>timers.get(nextThread)*2||!canLock(threadNumber)){
            //busy loop
        }
        this.start = System.nanoTime();
        this.finish = 0;
        int old = this.lockCount;
        this.lockCount = old +1;

    }

    private Boolean canLock(int threadNumber){
        if(this.turn == threadNumber){
            this.turn = threadNumber;
            return true;
        }
        return false;
    }

    @Override
    public void unlock(int threadNumber) {
        if(threadNumber == this.turn) {
            this.finish = System.nanoTime();
            Long original = this.timers.get(threadNumber);
            Long timed = (this.finish-this.start);
            long timer = original+timed;
            this.timers.set(threadNumber,timer);
           // System.out.println("thread: "+threadNumber+" time so far: "+this.timers.get(threadNumber));
            int old = this.lockCount;
            this.lockCount = old -1;
            if(this.lockCount == 0) {
                if (threadNumber == 1) {
                    this.turn = 0;
                } else {
                    this.turn = 1;
                }
            }

        }
    }


}
