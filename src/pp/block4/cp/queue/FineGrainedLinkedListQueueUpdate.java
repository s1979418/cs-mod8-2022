package pp.block4.cp.queue;

import net.jcip.annotations.ThreadSafe;
import pp.block2.cp.queue.Queue;
import pp.block2.cp.queue.QueueEmptyException;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Implementation of the LinkedList{@link Queue} which uses coarse grained locking.
 */
@ThreadSafe
public class FineGrainedLinkedListQueueUpdate<T> implements Queue<T> {
    public ReentrantReadWriteLock headLock = new ReentrantReadWriteLock(true);
    public ReentrantReadWriteLock tailLock = new ReentrantReadWriteLock(true);
    /**
     * The head of the linked list queue.
     * Points to the first element of the list.
     */
    private volatile LinkedListNode<T> head = null;
    /**
     * The tail of the linked list queue.
     * Points to the last element of the list.
     */
    private volatile LinkedListNode<T> tail = null;
    /**
     * Length of the list, cached for performance.
     */
    private final AtomicInteger length = new AtomicInteger();

    @Override
    public void push(T x) {
        LinkedListNode<T> temp = new LinkedListNode<>(x);
        try {
            //lock to check if head is null and write if needed
            headLock.writeLock().lock();
            //lock to write to the tail
            tailLock.writeLock().lock();
            if (this.head == null) {
                this.head = temp;
                headLock.writeLock().unlock();
            } else {
                //no longer need to write to head
                headLock.writeLock().unlock();
                //lock to write to the tail
                this.tail.setNext(temp);
            }
            this.tail = temp;
        } finally {
            //make sure all locks are unlocked
            try {
                headLock.readLock().unlock();
            } catch (IllegalMonitorStateException ignored) {
            }
            try {
                headLock.readLock().unlock();
            } catch (IllegalMonitorStateException ignored) {
            }
            try {
                tailLock.writeLock().unlock();
            } catch (IllegalMonitorStateException ignored) {
            }
            this.length.incrementAndGet();
        }

    }

    @Override
    public T pull() throws QueueEmptyException {
        try {
            headLock.readLock().lock();
            if (this.head != null) {
                T x = this.head.getContent();
                LinkedListNode<T> n = null;
                headLock.readLock().unlock();
                try {
                    headLock.writeLock().lock();
                    try {
                        n = this.head.getNext();

                    } catch (NullPointerException ignored) {
                    }

                    this.head = n;
                } finally {
                    try {
                        headLock.writeLock().unlock();
                    } catch (IllegalMonitorStateException ignored) {
                    }
                }

                return x;
            } else {
                throw new QueueEmptyException();
            }
        } finally {
            //make sure all locks are released
            try {
                headLock.readLock().unlock();
            } catch (IllegalMonitorStateException ignored) {
            }
            try {
                headLock.writeLock().unlock();
            } catch (IllegalMonitorStateException ignored) {
            }
        }


    }

    @Override
    public int getLength() {
        return this.length.get();
    }
}
