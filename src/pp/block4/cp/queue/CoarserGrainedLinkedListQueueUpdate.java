package pp.block4.cp.queue;

import net.jcip.annotations.ThreadSafe;
import pp.block2.cp.queue.Queue;
import pp.block2.cp.queue.QueueEmptyException;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Implementation of the LinkedList{@link Queue} which uses coarse grained locking.
 */
@ThreadSafe
public class CoarserGrainedLinkedListQueueUpdate<T> implements Queue<T> {
    public final Lock headLock = new ReentrantLock();
    /**
     * The head of the linked list queue.
     * Points to the first element of the list.
     */
    private volatile LinkedListNode<T> head = null;
    /**
     * The tail of the linked list queue.
     * Points to the last element of the list.
     */
    private volatile LinkedListNode<T> tail = null;
    /**
     * Length of the list, cached for performance.
     */
    private final AtomicInteger length = new AtomicInteger();

    @Override
    public void push(T x) {
        LinkedListNode<T> temp = new LinkedListNode<>(x);
        try {
            headLock.lock();
            if (this.head == null) {
                this.head = temp;
            } else {
                this.tail.setNext(temp);
            }
            this.tail = temp;
        } finally {
            headLock.unlock();
        }


    }

    @Override
    public T pull() throws QueueEmptyException {
        try {
            headLock.lock();
            if (this.head != null) {
                T x = this.head.getContent();
                LinkedListNode<T> n;
                try {
                    n = this.head.getNext();

                } catch (NullPointerException ignored) {
                    n = null;
                }
                this.head = n;

                return x;
            } else {
                throw new QueueEmptyException();
            }
        } finally {
            headLock.unlock();
        }
    }

    @Override
    public int getLength() {
        return this.length.get();
    }
}
