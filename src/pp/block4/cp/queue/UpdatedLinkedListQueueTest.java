package pp.block4.cp.queue;

import nl.utwente.pp.cp.junit.ConcurrentRunner;
import nl.utwente.pp.cp.junit.ThreadNumber;
import nl.utwente.pp.cp.junit.Threaded;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import pp.block2.cp.queue.Queue;
import pp.block2.cp.queue.QueueEmptyException;
import pp.block2.cp.unsafesequence.SafeLockSequence;
import pp.block2.cp.unsafesequence.Sequence;

import java.util.HashSet;
import java.util.Set;


@RunWith(ConcurrentRunner.class)
public class UpdatedLinkedListQueueTest {

    /**
     * The amount of threads used in each test.
     */
    private static final int AMOUNT_OF_THREADS = 10;
    private static final int items = 100;

    /**
     * Queue used for the producer consumer example (advancedMultiThreadedTest).
     */
    private Queue<Integer> originalProducerConsumerQueue;
    private Queue<Integer> coarserProducerConsumerQueue;
    private MultiLinkedListQueue<Integer> multiProducerConsumerQueue;

    private Sequence producingSequence;

    private Set<Object> concurChecker;

    private long startTime;
    private long finishTime;

    /**
     * If you need to set up some object before the multithreaded tests start, you can do it in a method annotated with
     * {@link Before}. These methods are always executed single threaded.
     */
    @Before
    public void before() {
        //Setup an empty queue.
        this.originalProducerConsumerQueue = new FineGrainedLinkedListQueueUpdate<>();
        this.coarserProducerConsumerQueue = new CoarserGrainedLinkedListQueueUpdate<>();
        this.multiProducerConsumerQueue = new MultiLinkedListQueue<>(AMOUNT_OF_THREADS);
        //set up the sequence
        this.producingSequence = new SafeLockSequence();
        //set up the set to check against
        this.concurChecker = new HashSet<>();

        this.startTime = System.nanoTime();
        this.finishTime = 0;
    }

    /**
     * Simple multithreaded test which performs reads and writes to a queue from the same threads. There is no
     * difference between the task of each thread.
     */
    @Test
    @Threaded(count = AMOUNT_OF_THREADS)
    public void originalSimpleMultiThreadedTest() throws InterruptedException {
        for (int i = 0; i < items; i++) {
            int write = producingSequence.getNext();
            this.originalProducerConsumerQueue.push(write);
            synchronized (this) {
                this.concurChecker.add(write);
            }
        }
        int excepts = 0;
        int i = items;
        while (i > 0) {
            if(excepts>items%2){
                Assert.fail("throwing too many empty queue exceptions in simpleThreadedTest");
            }
            Object read = null;
            try {
                read = this.originalProducerConsumerQueue.pull();
            } catch (QueueEmptyException e) {
                excepts++;
                Thread.sleep(500);
            }
            if (read != null) {
                synchronized (this) {
                    this.concurChecker.add(read);

                }
                i--;
            }


        }
    }
    /**
     * Simple multithreaded test which performs reads and writes to a queue from the same threads. There is no
     * difference between the task of each thread.
     */
    @Test
    @Threaded(count = AMOUNT_OF_THREADS)
    public void coarserSimpleMultiThreadedTest() throws InterruptedException {
        for (int i = 0; i < items; i++) {
            int write = producingSequence.getNext();
            this.coarserProducerConsumerQueue.push(write);
            synchronized (this) {
                this.concurChecker.add(write);
            }
        }
        int excepts = 0;
        int i = items;
        while (i > 0) {
            if(excepts>items%2){
                Assert.fail("throwing too many empty queue exceptions in simpleThreadedTest");
            }
            Object read = null;
            try {
                read = this.coarserProducerConsumerQueue.pull();
            } catch (QueueEmptyException e) {
                excepts++;
                Thread.sleep(500);
            }
            if (read != null) {
                synchronized (this) {
                    this.concurChecker.add(read);

                }
                i--;
            }


        }
    }

    /**
     * Simple multithreaded test which performs reads and writes to a queue from the same threads. There is no
     * difference between the task of each thread.
     */
    @Test
    @Threaded(count = AMOUNT_OF_THREADS)
    public void multiSimpleMultiThreadedTest(@ThreadNumber int threadNumber) throws InterruptedException {
        for (int i = 0; i < items; i++) {
            int write = producingSequence.getNext();
            this.multiProducerConsumerQueue.push(write,threadNumber);
            synchronized (this) {
                this.concurChecker.add(write);
            }
        }
        int excepts = 0;
        int i = items;
        while (i > 0) {
            if(excepts>items%2){
                Assert.fail("throwing too many empty queue exceptions in simpleThreadedTest");
            }
            Object read = null;
            try {
                read = this.multiProducerConsumerQueue.pull(threadNumber);
            } catch (QueueEmptyException e) {
                excepts++;
                Thread.sleep(500);
            }
            if (read != null) {
                synchronized (this) {
                    this.concurChecker.add(read);

                }
                i--;
            }


        }
    }
    /**
     * Function which reads integers from the queue and therefore performs the task of a consumer.
     *
     * @throws InterruptedException If the thread got interrupted.
     */
    private void originalConsumer() throws InterruptedException {
        int i = items;
        int excepts = 0;
        while (i > 0) {
            if(excepts>items){
                Assert.fail("throwing too many empty queue exceptions in consumer");
            }
            Object read = null;
            try {
                read = this.originalProducerConsumerQueue.pull();
            } catch (QueueEmptyException e) {
                excepts++;
                Thread.sleep(500);
            }
            if (read != null) {
                synchronized (this) {
                    this.concurChecker.add(read);
                }
                i--;
            }


        }
    }

    /**
     * Function which writes integers to the queue and therefore performs the task of a producer.
     *
     */
    private void originalProducer() {
        for (int i = 0; i < items; i++) {
            int write = producingSequence.getNext();
            this.originalProducerConsumerQueue.push(write);
            synchronized (this) {
                this.concurChecker.add(write);
            }
        }
    }

    /**
     * Function which reads integers from the queue and therefore performs the task of a consumer.
     *
     * @throws InterruptedException If the thread got interrupted.
     */
    private void coarserConsumer() throws InterruptedException {
        int i = items;
        int excepts = 0;
        while (i > 0) {
            if(excepts>items){
                Assert.fail("throwing too many empty queue exceptions in consumer");
            }
            Object read = null;
            try {
                read = this.coarserProducerConsumerQueue.pull();
            } catch (QueueEmptyException e) {
                excepts++;
                Thread.sleep(500);
            }
            if (read != null) {
                synchronized (this) {
                    this.concurChecker.add(read);
                }
                i--;
            }


        }
    }

    /**
     * Function which writes integers to the queue and therefore performs the task of a producer.
     *
     */
    private void coarserProducer() {
        for (int i = 0; i < items; i++) {
            int write = producingSequence.getNext();
            this.coarserProducerConsumerQueue.push(write);
            synchronized (this) {
                this.concurChecker.add(write);
            }
        }
    }


    /**
     * Function which reads integers from the queue and therefore performs the task of a consumer.
     *
     * @throws InterruptedException If the thread got interrupted.
     */
    private void multiConsumer(int thread) throws InterruptedException {
        int i = items;
        int excepts = 0;
        while (i > 0) {
            if(excepts>items){
                Assert.fail("throwing too many empty queue exceptions in consumer");
            }
            Object read = null;
            try {
                read = this.multiProducerConsumerQueue.pull(thread);
            } catch (QueueEmptyException e) {
                excepts++;
                Thread.sleep(500);
            }
            if (read != null) {
                synchronized (this) {
                    this.concurChecker.add(read);
                }
                i--;
            }


        }
    }

    /**
     * Function which writes integers to the queue and therefore performs the task of a producer.
     *
     */
    private void multiProducer(int thread) {
        for (int i = 0; i < items; i++) {
            int write = producingSequence.getNext();
            this.multiProducerConsumerQueue.push(write,thread);
            synchronized (this) {
                this.concurChecker.add(write);
            }
        }
    }


    /**
     * Test which shows a simple producer consumer pattern, with different threads performing different tasks.
     *
     * @param theadNumber The number of the thread executing the code.
     * @throws InterruptedException If one of the threads got interrupted.
     */
    @Test
    @Threaded(count = AMOUNT_OF_THREADS * 2)
    public void originalAdvancedMultiThreadedTest(@ThreadNumber int theadNumber) throws InterruptedException {
        if (theadNumber < AMOUNT_OF_THREADS) {
            this.originalProducer();
        } else {
            this.originalConsumer();
        }
    }

    /**
     * Test which shows a simple producer consumer pattern, with different threads performing different tasks.
     *
     * @param theadNumber The number of the thread executing the code.
     * @throws InterruptedException If one of the threads got interrupted.
     */
    @Test
    @Threaded(count = AMOUNT_OF_THREADS * 2)
    public void coarserAdvancedMultiThreadedTest(@ThreadNumber int theadNumber) throws InterruptedException {
        if (theadNumber < AMOUNT_OF_THREADS) {
            this.coarserProducer();
        } else {
            this.coarserConsumer();
        }
    }

    /**
     * Test which shows a simple producer consumer pattern, with different threads performing different tasks.
     *
     * @param theadNumber The number of the thread executing the code.
     * @throws InterruptedException If one of the threads got interrupted.
     */
    @Test
    @Threaded(count = AMOUNT_OF_THREADS * 2)
    public void multiAdvancedMultiThreadedTest(@ThreadNumber int theadNumber) throws InterruptedException {
        if (theadNumber < AMOUNT_OF_THREADS) {
            this.multiProducer(theadNumber);
        } else {
            this.multiConsumer(theadNumber%AMOUNT_OF_THREADS);
        }
    }


    /**
     * This test is missing the {@link Threaded} annotation, so it will run single threaded, like a normal JUnit test
     * would, which does not run on separate threads at all.
     */
    @Test
    public void originalSingleThreadedTest() {
        int write = producingSequence.getNext();
        this.originalProducerConsumerQueue.push(write);
        Object read;
        try {
            read = this.originalProducerConsumerQueue.pull();
        } catch (QueueEmptyException e) {
            throw new RuntimeException(e);
        }
        Assert.assertEquals(read, producingSequence.getNext() - 1);
    }

    /**
     * This test is missing the {@link Threaded} annotation, so it will run single threaded, like a normal JUnit test
     * would, which does not run on separate threads at all.
     */
    @Test
    public void coarserSingleThreadedTest() {
        int write = producingSequence.getNext();
        this.coarserProducerConsumerQueue.push(write);
        Object read;
        try {
            read = this.coarserProducerConsumerQueue.pull();
        } catch (QueueEmptyException e) {
            throw new RuntimeException(e);
        }
        Assert.assertEquals(read, producingSequence.getNext() - 1);
    }

    /**
     * This test is missing the {@link Threaded} annotation, so it will run single threaded, like a normal JUnit test
     * would, which does not run on separate threads at all.
     */
    @Test
    public void multiSingleThreadedTest() {
        int write = producingSequence.getNext();
        this.multiProducerConsumerQueue.push(write);
        Object read;
        try {
            read = this.multiProducerConsumerQueue.pull();
        } catch (QueueEmptyException e) {
            throw new RuntimeException(e);
        }
        Assert.assertEquals(read, producingSequence.getNext() - 1);
    }

    /**
     * If you want to assert something about the state of the class after all threads are ended, you can annotate
     * a method with {@link After} and assert in there. Methods annotated with {@link After} always run single threaded.
     */
    @After
    public void after() {
        //check how long the queue took
        this.finishTime = System.nanoTime();
        System.out.printf("Total execution time: %s ms.%n",
                (this.finishTime - this.startTime) / 1000000);
        //check that all elements were pushed and pulled correctly
        if (this.concurChecker.size() > 0) {
            Assert.assertEquals(items * AMOUNT_OF_THREADS, this.concurChecker.size());
        }
        //Assert the queues are empty.
        Object read = null;
        try {
            read = this.originalProducerConsumerQueue.pull();
        } catch (QueueEmptyException e) {
            Assert.assertTrue(true);
        }
        if (read != null) {
            Assert.fail("still something in the original queue namely: " + read);
        }

        Object read2 = null;
        try {
            read2 = this.coarserProducerConsumerQueue.pull();
        } catch (QueueEmptyException e) {
            Assert.assertTrue(true);
        }
        if (read2 != null) {
            Assert.fail("still something in the coarser queue namely: " + read2);
        }

        Object read3 = null;
        for(int i =0;i<AMOUNT_OF_THREADS;i++) {
            try {

                read3 = this.multiProducerConsumerQueue.pull(i);

            } catch (QueueEmptyException e) {
                Assert.assertTrue(true);
            }
            if (read3 != null) {
                Assert.fail("still something in the original queue namely: " + read3);
            }
        }
    }

}


