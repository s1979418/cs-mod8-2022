package pp.block4.cp.queue;

import pp.block2.cp.queue.Queue;
import pp.block2.cp.queue.QueueEmptyException;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MultiLinkedListQueue<T> implements Queue<T> {
    /**
     * Length of the list, cached for performance.
     */
    private final AtomicInteger length = new AtomicInteger();

    Map<Integer, LinkedList<T>> queues = new HashMap<>();
    Map<Integer, Lock> locks = new HashMap<>();

    public MultiLinkedListQueue(int numbOfQueues) {
        for(int i = 0 ; i < numbOfQueues ; i++){
            LinkedList<T> list = new LinkedList<>();
            queues.put(i,list);
            Lock lock = new ReentrantLock();
            locks.put(i,lock);
        }
    }

    @Override
    public void push(T x){
        push(x,0);
    }
    public void push(T x, int queNum) {
        LinkedList<T> neededList = queues.get(queNum);
        Lock neededLock = locks.get(queNum);
        try {
            neededLock.lock();
            neededList.push(x);
        }finally {
            neededLock.unlock();
        }

    }

    @Override
    public T pull() throws QueueEmptyException {
        return pull(0);
    }
    public T pull(int queNum) throws QueueEmptyException {
        LinkedList<T> neededList = queues.get(queNum);
        Lock neededLock = locks.get(queNum);
        try {
            neededLock.lock();
            if (!neededList.isEmpty()) {

                return neededList.pop();
            } else {
                throw new QueueEmptyException();
            }
        }finally {
            neededLock.unlock();
        }

    }

    @Override
    public int getLength() {
        return length.get();
    }
}
