package pp.block4.cp.copyonwrite;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CustomCopyOnWriteArrayList<V> {
	public final Lock writeLock = new ReentrantLock();
	private ArrayList<V> list = new ArrayList<>();

	/**
	 * Set the value at the specified index.
	 * @param index The index to set the value at.
	 * @param value The value to set at the specified index.
	 * @return The old value on the specified index
	 */
	public V set(int index, V value) {
		try {
			writeLock.lock();
			ArrayList<V> newList = new ArrayList<>(list);
			V old = newList.get(index);
			newList.set(index,value);
			list = newList;
			return old;
		}finally {
			writeLock.unlock();
		}
	}

	/**
	 * Add a value to the end of the list.
	 * @param value The value to add to the end of the list.
	 */
	public void add(V value) {
		try {
			writeLock.lock();
			ArrayList<V> newList = new ArrayList<>(list);
			newList.add(value);
			list = newList;
		}finally {
			writeLock.unlock();
		}
	}

	/**
	 * Get the value at the specified index.
	 * @param index The index to get the value of.
	 * @return The value at the specified index.
	 */
	public V get(int index) {
		return list.get(index);
	}
}