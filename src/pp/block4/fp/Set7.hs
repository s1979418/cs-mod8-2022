{-# LANGUAGE TemplateHaskell #-}
import Test.QuickCheck
import FP_Core


--ex 4.2
codeGen :: Expr -> [Instr]
codeGen ex = codeGenhelp ex ++ [EndProg]

codeGenhelp :: Expr -> [Instr]
codeGenhelp (Const x) = [PushConst x]
codeGenhelp (BinExpr op x y) = codeGenhelp x ++ codeGenhelp y ++ [Calc op]




return []
check = $quickCheckAll