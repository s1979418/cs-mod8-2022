goal(state(_,_,_,has)).
init(state(atdoor,onfloor,atwindow,hasnot)).

move(state(Pos,onfloor,Pos,Has),climb,state(Pos,onbox,Pos,Has)).
move(state(middle,onbox,middle,hasnot),grab,state(middle,onbox,middle,has)).
move(state(Pos1,onfloor,Pos1,Has),push(Pos1,Pos2),state(Pos2,onfloor,Pos2,Has)).
move(state(Pos1,onfloor,Box,Has),walk(Pos1,Pos2),state(Pos2,onfloor,Box,Has)).

solve(State1,_):- goal(State1).
solve(State1,[Move|Moves]):-move(State1,Move,State2),solve(State2,Moves),print(Move),nl.