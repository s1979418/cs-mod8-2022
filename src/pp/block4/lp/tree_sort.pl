%4.12
istree(nil).
istree(t(L,N,R)):- istree(L),istree(R).

%4.13
max(t(_,Nt,nil),N):- N=Nt.
max(t(_,Nt,R),N):- max(R,N).

min(t(nil,Nt,_),N):- N=Nt.
min(t(L,Nt,_),N):- min(L,N).

%4.14
issorted(nil).
issorted(t(nil,N,nil)).
issorted(t(nil,N,t(L,Nr,R))) :- N =< Nr.
issorted(t(t(L,Nl,R),N,nil)) :- N > Nl.
issorted(t(t(Ll,Nl,Rl),Nt,t(Lr,Nr,Rr))):- Nt > Nl, Nt =< Nr, issorted(t(Ll,Nl,Rl)), issorted(t(Lr,Nr,Rr)).

%4.15
find(nil,N,S):-fail.
find(t(nil,Nt,nil),N,S):- N =:= Nt, S=t(nil,Nt,nil).
find(t(L,Nt,R),N,S):- N=:=Nt,S=t(L,Nt,R).
find(t(L,Nt,R),N,S):- N<Nt,find(L,N,S).
find(t(L,Nt,R),N,S):- N>Nt,find(R,N,S).

%4.16
insert(nil,N,S):- S=t(nil,N,nil).
insert(t(L,Nt,R),N,S):- N=:=Nt,S=t(L,Nt,t(nil,N,R)).
insert(t(L,Nt,R),N,S):- N<Nt,insert(L,N,Si),S=t(Si,Nt,R).
insert(t(L,Nt,R),N,S):- N>Nt,insert(R,N,Si),S=t(L,Nt,Si).

%4.17
deleteAll(nil,N,S):- S=nil.
deleteAll(t(nil,Nt,nil),N,S):- (N =:= Nt, S=nil); (N \= Nt,S=t(nil,Nt,nil)).

deleteAll(t(nil,Nt,R),N,S):- N=:=Nt , deleteAll(R,N,Sd), S=Sd.
deleteAll(t(L,Nt,R),N,S):- N =:= Nt , deleteAll(R,N,nil),S= L.
deleteAll(t(L,Nt,R),N,S):- N =:= Nt , deleteAll(R,N,Sd),max(L,Nl),deleteOne(L,Nl,Sl),S= t(Sl,Nl,Sd).

deleteAll(t(L,Nt,R),N,S):- N > Nt, deleteAll(R,N,Sd), S= t(L,Nt,Sd).
deleteAll(t(L,Nt,R),N,S):- N < Nt, deleteAll(L,N,Sd), S= t(Sd,Nt,R).


deleteOne(nil,N,S):- S=nil.
deleteOne(t(nil,Nt,nil),N,S):- (N =:= Nt, S=nil); (N \= Nt,S= t(nil,Nt,nil)).

deleteOne(t(L,Nt,t(Lr,Nr,Rr)),N,S):- N =:= Nt, N =:= Nr, deleteOne(R,N,Sd), S= t(L,Nt,Sd).
deleteOne(t(L,Nt,t(Lr,Nr,Rr)),N,S):- N =:= Nt, N \= Nr, min(t(Lr,Nr,Rr),Mr),deleteOne(t(Lr,Mr,Rr),Mr,Sr),S= t(L,Mr,Sr).

deleteOne(t(L,Nt,R),N,S):- N > Nt, deleteOne(R,N,Sd), S= t(L,Nt,Sd).
deleteOne(t(L,Nt,R),N,S):- N < Nt, deleteOne(L,N,Sd), S= t(Sd,Nt,R).


%4.18
listtree([],T):- T = nil.
listtree([X],T):- T = t(nil,X,nil).
listtree([X|XS],T):- listtree(XS,Tn), insert(Tn,X,S), T=S.

%4.19
treelist(nil,L):- L=[].
treelist(T,L):- min(T,N), deleteAll(T,N,Tnew),treelist(Tnew,Lnew), L=[N|Lnew].

%4.20

treelistone(nil,L):- L=[].
treelistone(T,L):- min(T,N), deleteOne(T,N,Tnew),treelistone(Tnew,Lnew), L=[N|Lnew].

treesort(T1,T2):- treelistone(T1,L),listtree(L,T2).

treesort(t(t(t(t(nil, 1, nil), 2, nil), 3, t(nil, 7, nil)), 8, t(nil, 10, t(nil, 11, nil))),L).
