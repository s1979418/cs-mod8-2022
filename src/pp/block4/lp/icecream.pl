%4.24

%model: stand(Day,Name,Location,Icetype)
%       Day can be tuesday, wednesday, thursday,friday
%       Name can be arend,jaco,marco,marieke
%       Location can be espelo,twickel, weldam, westerflier
%       Icetype can be chocochip,coffee,peanut,peppermint

stand(_,_,_,_).

% show which days are after eachother
nextday(tuesday,wednesday).
nextday(wednesday,thursday).
nextday(thursday,friday).

member(X,[X|_]).
member(X,[_|L]) :- member(X,L).

% tour list of stands as given
tour(Week):- Week = [stand(tuesday,_,_,_)
                ,stand(wednesday,_,_,_)
                ,stand(thursday,_,_,_)
                ,stand(friday,_,_,_)], 

        member(stand(DayArend,arend,_,_),Week),
        member(stand(DayTwickel,_,twickel,_),Week), 
        not(DayTwickel = DayArend),
        member(stand(Day3,_,_,peppermint),Week),
        not(Day3=thursday),

        member(stand(wednesday,_,_,coffee),Week),
        member(stand(Day4,marieke,_,_),Week),
        not(Day4=wednesday),

        member(stand(Day5,jaco,_,peanut),Week),
        not(Day5=tuesday),

        member(stand(DayWest,_,westerflier,_),Week),
        member(stand(DayChoc,_,_,chocochip),Week),
        member(stand(DayMarco,marco,_,_),Week),
        nextday(DayWest,DayChoc),
        nextday(DayMarco,DayWest),

        member(stand(DayWeldam,_,weldam,_),Week),
        nextday(DayWeldam,DayArend),

        member(stand(_,_,espelo,_),Week).


go(X):- tour(X).


