{-# LANGUAGE TemplateHaskell #-}
module Set5 where

import Data.List
import Data.Functor
import Data.Either

import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token

import Test.QuickCheck

fromLeft' :: Either l r -> l
fromLeft' (Left x) = x -- Newer GHC versions contain a fromLeft :: l -> Either l r -> l

fromRight' :: Either l r -> r
fromRight' (Right x) = x -- Newer GHC versions contain a fromRight :: r -> Either l r -> r

parser :: Parser a -> String -> a
parser p xs | isLeft res = error $ show $ fromLeft' res
          | otherwise  = fromRight' res
  where res = parse p "" xs


--ex 3.1 examples from the lecture:

languageDef=
  emptyDef { Token.commentStart = "/*"
            , Token.commentEnd = "*/"
            , Token.commentLine = "//"
            , Token.identStart = letter
            , Token.identLetter = alphaNum
            , Token.reservedNames = [ "for"
                                    , "while"
                                    , "if"
                                    ]
            , Token.reservedOpNames = [ "-", "+", "*"]
  }
-- Expression EDSL
data Expr = Const Integer
          | IdEx String Expr
          | Var String 
          | Mult Expr Expr
          | Add Expr Expr
          | If Cond Expr Expr
          | Dec Expr
          | Par Expr
          deriving Show

--ex 3.2

lexer = Token.makeTokenParser languageDef
identifier = Token.identifier lexer
integer = Token.integer lexer
parens = Token.parens lexer
symbol = Token.symbol lexer
reserved = Token.reserved lexer

--ex 3.3
parseFactor :: Parser Expr
parseFactor = try (IdEx <$> identifier <*> parens parseExpr)
            <|> try (Const <$> integer) 
            <|> try (Var <$> identifier)
            <|> parens parseExpr
            
            

parseTerm :: Parser Expr
parseTerm = parseFactor `chainl1` mult

mult :: Parser (Expr -> Expr -> Expr)
mult = (\_ -> Mult) <$> symbol "*"

parseExpr :: Parser Expr
parseExpr = parseDec 
        <|> parseIf
        <|> parseTerm `chainl1` ad 

ad :: Parser (Expr -> Expr -> Expr)
ad = (\_ -> Add) <$> symbol "+"

-- ex 3.4
data Cond = Equal Expr Expr
            deriving Show

parseCondition :: Parser Cond 
parseCondition = Equal <$> parseExpr <*> (symbol "==" *> parseExpr)

parseIf :: Parser Expr
parseIf = If <$> (symbol "if" *> parseCondition) <*> (symbol "then" *> parseExpr) <*> (symbol "else" *> parseExpr)

parseDec :: Parser Expr
parseDec = Dec <$> (symbol "dec" *> parseExpr)

--ex 3.5
data FunDef = Func String String Expr 
            deriving (Show)

parseFunction :: Parser FunDef
parseFunction = Func <$> (symbol "function" *> identifier) <*> identifier <*> (symbol "=" *> parseExpr)

parserFun :: String -> FunDef
parserFun = parser parseFunction 

fib' :: FunDef
fib' = parserFun "function fib x = if x == 0 then 1 else ( if x == 1 then 1 else fib ( dec x )+ fib ( dec dec x )) "

--ex 3.6
evalfun :: FunDef -> Integer -> Integer 
evalfun (Func name var def) x = evalexpr def x (Func name var def)

evalexpr :: Expr -> Integer -> FunDef -> Integer
evalexpr (Const a) x f = a 
evalexpr (IdEx name var) x f= evalfun f (evalexpr var x f)
evalexpr (Var var ) x f= x
evalexpr (Mult ex1 ex2) x f= (evalexpr ex1 x f) * (evalexpr ex2 x f)
evalexpr (Add ex1 ex2) x f= (evalexpr ex1 x f) + (evalexpr ex2 x f)
evalexpr (If cond the els) x f
                      | evalcond cond x f = evalexpr the x f
                      | otherwise = evalexpr els x f
evalexpr (Dec ex) x f= (evalexpr ex x f) -1
evalexpr (Par ex) x f= evalexpr ex x f

evalcond :: Cond -> Integer -> FunDef -> Bool
evalcond (Equal ex1 ex2) x f= (evalexpr ex1 x f) == (evalexpr ex2 x f)


correctfun :: FunDef
correctfun = parser parseFunction
  "function f x = if x == 0 then 1 else y+g (0) "

fib :: Integer -> Integer
fib = (evalfun . parserFun)
 "function fib x = if x == 0 then 1 else (if x == 1 then 1 else fib(dec x)+fib(dec dec x))"

-- Checks if in the evaluation (xˆ2 + 2*x + 1) == (x+1)ˆ2
fn = ( evalfun . parserFun ) "function f x = x*x + 2* x + 1"
fn' = ( evalfun . parserFun ) "function f x = (x +1) * (x +1) "
prop_fn n = n >= 0 ==> fn n == fn' n

factorial :: Integer -> Integer
factorial = ( evalfun . parserFun )
  "function factorial x = if x == 0 then 1 else factorial ( dec x) * x"
prop_factorial n = n >= 0 ==> factorial n == product [1.. n]

return []
check = $quickCheckAll