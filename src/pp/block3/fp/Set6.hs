{-# LANGUAGE TemplateHaskell #-}
module Set6 where

import Test.QuickCheck
import Data.Foldable
import Data.Monoid
import Text.Read
import Control.Applicative
import Set5


--ex 3.7
addstr :: String -> String -> Maybe String
addstr xs ys = show <$> ((+) <$> (readMaybe xs) <*> (readMaybe ys)) 

------------------------------------------------------------------------------
-- Exercise 3-FP.8
------------------------------------------------------------------------------


data MyList a = Nil | Cons a (MyList a)
              deriving (Show, Eq)

mylst  = Cons 1   $ Cons 2   $ Cons 3   $ Nil
mylst2 = Cons 10  $ Cons 20  $ Cons 30  $ Nil
mylst3 = Cons 100 $ Cons 200 $ Cons 300 $ Nil

myzipWith3' :: (a -> b -> c -> d) -> MyList a -> MyList b -> MyList c -> MyList d
myzipWith3' f Nil _ _ = Nil
myzipWith3' f _ Nil _ = Nil
myzipWith3' f _ _ Nil = Nil
myzipWith3' f (Cons x xs) (Cons y ys) (Cons z zs) = Cons (f x y z) $ (myzipWith3' f xs ys zs)

instance Functor MyList where
    fmap f Nil = Nil
    fmap f (Cons x xs) = (Cons (f x)) $ fmap f xs

instance Applicative MyList where
    pure x = Cons x $ Nil
    Nil <*> (Cons x xs) = Nil
    fs <*> Nil = Nil
    (Cons f fs) <*> (Cons x xs) = (Cons (f x)) $ (fs <*> xs)

--ex 3.9
getInt :: IO Integer
getInt = fmap read getLine

f :: IO Integer
f = (+) <$> getInt <*> getInt 

--ex 3.10
justs :: [Maybe a] -> Maybe [a]
justs [] = pure []
justs (x:xs) = (:) <$> x <*> (justs xs)

--ex 3.11
data Parser r = P {
                    runParser :: String -> [(r , String)]
                    }

char :: Char -> Parser Char
char c = P p
        where
            p [] = []
            p (x: xs ) 
                    | c == x = [(x , xs )]
                    | otherwise = []

parseOne :: Parser Char
parseOne = char '1'

testOne :: String -> Bool
testOne (x:xs) 
    | x == '1' = runParser parseOne (x:xs) == [('1',xs)]
    | otherwise = runParser parseOne (x:xs) == []

instance Functor Parser where 
    fmap f (P a) = P (\x -> [(f r,s)|(r,s) <- a x])

parseOneInt :: Parser Int
parseOneInt = fmap (\x -> read [x]) parseOne

testOneInt :: String -> Bool
testOneInt (x:xs) 
    | x == '1' = runParser parseOneInt (x:xs) == [(1,xs)]
    | otherwise = runParser parseOneInt (x:xs) == []

instance Applicative Parser where
    pure x = P (\y -> [(x,y)])
    pf <*> pa = P (\y -> [(f a, aaah)| (f, ffs) <- runParser pf y, (a,aaah) <- runParser pa ffs])

parseAB :: Parser (Char, Char)
parseAB = ( ,) <$> char 'a' <*> char 'b'

parseString :: String -> Parser String
parseString [] = pure []
parseString (x:xs) = (:) <$> char x <*> (parseString xs)

instance Alternative Parser where 
    empty = P (\x -> [])
    pb <|> pc = P (\y -> runParser pb y ++ runParser pc y)


------------------------------------------------------------------------------
-- Exercise 3-FP.12
------------------------------------------------------------------------------

fibonacci :: IO (Integer -> Integer)
fibonacci = (evalfun . parserFun) <$> readFile "fib.txt"

fib5 :: IO Integer
fib5 = fibonacci <*> pure 5

fibs :: IO [Integer]
fibs = (map <$> fibonacci ) <*> pure [0..]

fact = parserFun "function factorial x = if x == 0 then 1 else factorial(dec x) * x"

prompt :: IO Integer
prompt = evalfun <$> (pure fact) <*> getInt

calculations :: Integer -> [String] -> [Integer]
calculations x [] = []
calculations x (f:fs) =  fib ((evalfun . parserFun) f x) : (calculations x fs)

calcfile :: FilePath -> Integer -> IO [Integer]
calcfile path x = pure <*> show <$> (calculations <$> pure x <*> lines <$> readFile path)

--ex 3.13
data A = R [Int]
        | Q Int Char
        deriving Show

makeAR :: Gen A 
makeAR = R <$> (arbitrary :: Gen [Int])

makeAQ :: Gen A 
makeAQ = Q <$> (arbitrary :: Gen Int) <*> (arbitrary :: Gen Char)

instance Arbitrary A where 
    arbitrary = oneof [makeAR , makeAQ]

return []
check = $quickCheckAll