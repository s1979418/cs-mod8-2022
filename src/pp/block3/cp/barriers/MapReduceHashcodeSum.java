package pp.block3.cp.barriers;

import java.util.ArrayList;

public class MapReduceHashcodeSum extends MapReduceBase{
    @Override
    protected Object map(Object in) {
        return in.hashCode();
    }

    @Override
    protected Object reduce(ArrayList in) {
        Integer sum = 0;
        for(Object i : in){
            sum = sum + (Integer) i;
        }
        return sum;
    }
}
