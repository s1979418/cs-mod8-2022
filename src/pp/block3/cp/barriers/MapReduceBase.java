package pp.block3.cp.barriers; 

import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * Abstract class implementing the
 * map-reduce framework. To be completed
 * by the student.
 */
public abstract class MapReduceBase<I, O, R> {
	/**
	 * List for storing the results of all the
	 * map tasks
	 */
	private ArrayList<O> mapOutput;
	CyclicBarrier barrier;
	protected abstract O map(I in);
	protected abstract R reduce(ArrayList<O> in);

	private class RunMap implements Runnable {
		I elem;
		int index;

		public RunMap(I elem, int index) {
			this.elem = elem;
			this.index = index;
		}

		@Override
		public void run() {
			O res = map(this.elem);
			synchronized (mapOutput) {
				mapOutput.add(index, res);
			}
			// process one element using map-function and store result in mapOutput
			try {
				barrier.await();
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			} catch (BrokenBarrierException ignored) {
			}
		}
	}
	/**
	 * Runs the map-reduce operation
	 * @param values the values to operate on
	 * @return the result of the map-reduce operation
	 */
	protected R run(ArrayList <I> values) {
		mapOutput = new ArrayList<>(values.size());
		barrier = new CyclicBarrier(values.size() + 1);
		//spawn and start map threads to do the work
		for(int i = 0; i<values.size();i++){
			RunMap mapper = new RunMap(values.get(i),i);
			Thread helper = new Thread(mapper);
			helper.start();

		}
		//wait for all threads to be done
		try {
			barrier.await();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} catch (BrokenBarrierException e) {
			//reduce
			return reduce(mapOutput);
		}


		return null;
	}
}
