package pp.block3.cp.barriers;

import net.jcip.annotations.ThreadSafe;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

@ThreadSafe
public class MapReduceBaseTest {
    MapReduceHashcodeSum mapReducer;
    ArrayList<Object> list;
    Integer mappedSum;

    @Before
    public void before(){
        //intialise mapreducehashcode
        mapReducer = new MapReduceHashcodeSum();
        list = new ArrayList<>();
        for(int i = 0; i<10;i++){
            list.add(new Object());
        }

    }
    @Test
    public void test1(){
        mappedSum = (Integer) mapReducer.run(list);

    }
    @Test
    public void test2(){
        mappedSum = (Integer) mapReducer.run(list);

    }
    @Test
    public void test3(){
        mappedSum = (Integer) mapReducer.run(list);

    }
    @After
    public void after(){
        //check outcome on correctness
        Integer sum = 0;
        for(Object o : list){
            sum = sum + o.hashCode();
        }

    }
}
