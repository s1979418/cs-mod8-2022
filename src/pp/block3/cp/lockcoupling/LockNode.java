package pp.block3.cp.lockcoupling;

import net.jcip.annotations.NotThreadSafe;

import java.util.concurrent.locks.Lock;

/**
 * Simple node for synchronized list. 
 * Used for testing the performance in exercise 3 of block 3.
 */
@NotThreadSafe
public class LockNode<T> {
	/** The value of backed by this node. */
	private T item;

	/** The next node of the list. */
	private LockNode<T> next;

	private Lock lock;

	public LockNode(T value, LockNode<T> next, Lock lock) {
		this.item = value;
		this.next = next;
		this.lock = lock;
	}

	public T getItem() {
		return this.item;
	}

	public LockNode<T> getNext() {
		return this.next;
	}

	public void setNext(LockNode<T> next) {
		this.next = next;
	}

	public Lock getLock(){
		return this.lock;
	}
}
