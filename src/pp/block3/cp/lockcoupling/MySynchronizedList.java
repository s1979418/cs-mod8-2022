package pp.block3.cp.lockcoupling;

import net.jcip.annotations.ThreadSafe;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Synchronized list which just synchronizes on the whole list 
 * Used for testing performance in exercise 3 of block 3.
 */
@ThreadSafe
public class MySynchronizedList<T> implements List<T> {

	// The sentinel node points to the first element of the list.
	private final LockNode<T> sentinel = new LockNode<>(null, null,new ReentrantLock());

	@Override
	public void insert(int position, T value) {
		LockNode<T> current = this.find(position);
		try{
		current.getLock().lock();
		current.setNext(new LockNode<>(value, current.getNext(), new ReentrantLock()));
		}finally {
			current.getLock().unlock();
		}

	}

	@Override
	public void add(T value) {
		LockNode<T> current = this.sentinel;
		while (current.getNext() != null) {
			current = current.getNext();
		}
		try {
			current.getLock().lock();
			current.setNext(new LockNode<>(value, null, new ReentrantLock()));
		}finally {
			current.getLock().unlock();
		}
	}

	@Override
	public int size() {
		int count = 0;
		LockNode<T> current = this.sentinel;
		while (current.getNext() != null) {
			current.getLock().lock();
			count++;
			LockNode<T> temp = current.getNext();
			current.getLock().unlock();
			temp.getLock().lock();
			current = temp;
			temp.getLock().unlock();
		}
		return count;
	}

	@Override
	public void remove(T item) {
		LockNode<T> current = this.sentinel;
		LockNode<T> tmp;
		while (current.getNext() != null) {
			tmp = current;
			try {
				tmp.getLock().lock();
				current.getNext().getLock().lock();

				current = current.getNext();
				if (current.getItem().equals(item)) {
					tmp.setNext(current.getNext());
					return;
				}
			}finally{
				tmp.getLock().unlock();
				current.getLock().unlock();
			}
		}
	}

	@Override
	public void delete(int position) {
		LockNode<T> current = this.find(position);
		try {
			current.getLock().lock();
			if (current.getNext() != null) {
				current.setNext(current.getNext().getNext());
			}
		} finally{
			current.getLock().unlock();
		}
	}

	/**
	 * Find the element at the specified position.
	 * @param position The position to get the element of.
	 */
	private LockNode<T> find(int position) {
		LockNode<T> current = this.sentinel;
		while (current.getNext() != null && position > 0) {
			current = current.getNext();
			position--;
		}
		return current;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		LockNode<T> current = this.sentinel;
		while (current.getNext() != null) {
			try {
				current.getLock().lock();
				current = current.getNext();
				result.append(current.getItem().toString()).append(" ");
			}finally {
				current.getLock().unlock();
			}

		}
		return result.toString();
	}

}
