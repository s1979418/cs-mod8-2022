package pp.block3.cc.symbol;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class MySymbolTable implements SymbolTable{
    int currentScope;
    Stack<Map<String,Object>> scopeStack;
    public MySymbolTable() {
        currentScope = -1;
        scopeStack = new Stack<>();
    }

    public Stack<Map<String,Object>> getScopeStack(){
        return scopeStack;
    }


    @Override
    public void openScope() {
        if(currentScope == -1){
            currentScope = 0;
            scopeStack.push(new HashMap<>());
        }
        else {
            currentScope = currentScope + 1;
            scopeStack.push(new HashMap<>());
        }
    }

    @Override
    public void closeScope() {
        currentScope = currentScope -1;
        scopeStack.pop();
    }

    @Override
    public boolean add(String id) {
        try {
            Map<String, Object> currentMap = scopeStack.pop();
            if(currentMap.containsKey(id)){
                scopeStack.push(currentMap);
                return false;
            }
            currentMap.put(id, "");
            scopeStack.push(currentMap);
            return true;
        } catch (Exception e){
            if(e.getClass()==EmptyStackException.class){
                Map<String, Object> currentMap = new HashMap<>();
                currentMap.put(id, "");
                currentScope = currentScope +1;
                scopeStack.push(currentMap);
                return true;
            }
            System.out.print(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean contains(String id) {
        try {
            Stack<Map<String, Object>> helperStack = new Stack<>();
            boolean found = false;
            while(!found && !scopeStack.empty()) {
                Map<String, Object> currentMap = scopeStack.pop();
                helperStack.push(currentMap);
                if(currentMap.containsKey(id)){
                    found = true;
                }
            }
            while(!helperStack.empty()){
                scopeStack.push(helperStack.pop());
            }
            return found;
        } catch (Exception e){
            System.out.print("an error occurred: "+e.getMessage());
            return false;
        }
    }
}
