package pp.block3.cc.symbol;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import pp.block3.cc.symbol.DeclUseParser.*;

import java.util.ArrayList;
import java.util.List;

public class MyDeclUseListener  implements pp.block3.cc.symbol.DeclUseListener {
MySymbolTable symbolTable;
List<String> errors;
    public MyDeclUseListener() {
        symbolTable = new MySymbolTable();
        errors = new ArrayList<>();
    }

    public List<String> getErrors(){
        if(!errors.isEmpty()){
            System.out.println("There are "+errors.size()+" errors:");
            for(String m:errors){
                System.out.println(m);
            }
        }
        else {System.out.println("No errors found");}
        return errors;
    }
    @Override
    public void enterProgram(ProgramContext ctx) {
        symbolTable.openScope();
    }

    @Override
    public void exitProgram(pp.block3.cc.symbol.DeclUseParser.ProgramContext ctx) {
        try{
            symbolTable.closeScope();
            errors.add("too many scopes opened");
            System.out.print(symbolTable.getScopeStack());
        }catch (RuntimeException e){
            //
        }
        if(!errors.isEmpty()){
            System.out.println("There are "+errors.size()+" errors:");
            for(String m:errors){
                System.out.println(m);
            }
        }
        else{System.out.println("Program parsed successfully");}
    }

    @Override
    public void enterSeries(pp.block3.cc.symbol.DeclUseParser.SeriesContext ctx) {

    }

    @Override
    public void exitSeries(pp.block3.cc.symbol.DeclUseParser.SeriesContext ctx) {

    }

    @Override
    public void enterUnit(pp.block3.cc.symbol.DeclUseParser.UnitContext ctx) {
        if(ctx.getText().startsWith("(")){
            symbolTable.openScope();
        }
    }

    @Override
    public void exitUnit(pp.block3.cc.symbol.DeclUseParser.UnitContext ctx) {
        if(ctx.getText().endsWith(")")){
            try {
                symbolTable.closeScope();
            }catch (RuntimeException e){
                Token problem = ctx.getStart();
                errors.add("illegal closing of scope in "+ctx.getText()+" at line "+ problem.getLine()+" and column "+problem.getCharPositionInLine());
            }
        }
    }

    @Override
    public void enterDecl(pp.block3.cc.symbol.DeclUseParser.DeclContext ctx) {

    }

    @Override
    public void exitDecl(pp.block3.cc.symbol.DeclUseParser.DeclContext ctx) {
        boolean added = symbolTable.add(ctx.ID().getText());
        if(!added){
            Token problem = ctx.ID().getSymbol();
            errors.add("illegal double declaration of variable "+ctx.ID().getText()+" at line "+ problem.getLine()+" and column "+problem.getCharPositionInLine());
        }
    }

    @Override
    public void enterUse(pp.block3.cc.symbol.DeclUseParser.UseContext ctx) {

    }

    @Override
    public void exitUse(pp.block3.cc.symbol.DeclUseParser.UseContext ctx) {
        if(!symbolTable.contains(ctx.ID().getText())){
            Token problem = ctx.ID().getSymbol();
            errors.add("use without declaration of variable "+ctx.ID().getText()+" at line "+problem.getLine()+" and column "+problem.getCharPositionInLine());
        }
    }

    @Override
    public void visitTerminal(TerminalNode terminalNode) {

    }

    @Override
    public void visitErrorNode(ErrorNode errorNode) {
        errors.add("invalid code at line "+errorNode.getSymbol().getLine()+" and column "+errorNode.getSymbol().getCharPositionInLine());
    }

    @Override
    public void enterEveryRule(ParserRuleContext parserRuleContext) {

    }

    @Override
    public void exitEveryRule(ParserRuleContext parserRuleContext) {

    }
}
