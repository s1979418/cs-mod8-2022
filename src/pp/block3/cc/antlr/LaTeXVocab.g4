grammar LaTeXVocab;
WS : [ \t\n\r];
WSmin: [ \t];

COMMANDSTART:   WS* '\\' WS*;
ARGSTART:       WS* '{' WS*;
ARGEND:         WS* '}' WS*;
ROWEND:         WS* '\\\\' WS*;
ENTRYSEP:       WS* '&' WS*;
RESERVED:       WS* ('$'|'#'|'^'|'_'|'-') WS*;
COMMENTSTART:   WS* '%' WSmin*;

COMMENT:    COMMENTSTART (LETTERS|NUMBER|WSmin)* '\n' -> skip;

LETTERS :           [A-Z|a-z];
NUMBER :            [0-9]+;
WORD :              WS* (LETTERS|NUMBER)+ WS*;

fragment ALLCHARS:(LETTERS|NUMBER|COMMANDSTART|ARGSTART|ARGEND|ROWEND|ENTRYSEP|RESERVED|COMMENTSTART|'!'|'#'|'$'|'%'|'\''|'('|')'|'*'|'+'|'-'|','|'.'|'/'|':'|';'|'?'|'@'|'['|']'|'^'|'_'|'`'|'|'|'~');

TABSTARTCOM: COMMANDSTART'begin' ARGSTART 'tabular'ARGEND;
TABENDCOM: COMMANDSTART'end' ARGSTART 'tabular' ARGEND EOF;

fragment L: 'l';
fragment C: 'c';
fragment R: 'r';

ALIGNMENT: WS* (L|C|R) WS*;
