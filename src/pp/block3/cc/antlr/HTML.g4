grammar HTML;

WS : [ \t\n\r];

COMMANDSTART :      WS* '\\' WS* ;
COMMANDARGSTART :   WS* '{' WS* ;
COMMANDARGEND :     WS* '}' WS* ;
ENTRYSEP :          WS* '&' WS* ;
COMMENT :           WS* '%' WS* ;
TAGSTART :          WS* '<' WS* ;
TAGEND :            WS* '>' WS* ;
ENDTAG :            WS* '</' WS* ;
ASSIGN :            WS* '=' WS* ;
QUOTE :             WS* '"' WS* ;

LETTERS :           [A-Z|a-z];
NUMBER :            [0-9]+;
WORD :              WS* (LETTERS|NUMBER)+ WS*;

HTMLSTARTTAG :      TAGSTART 'html' TAGEND;
HTMLENDTAG :        ENDTAG 'html' TAGEND;
BODYSTARTTAG :      TAGSTART 'body' TAGEND;
BODYENDTAG :        ENDTAG 'body' TAGEND;

TABLESTARTTAG :     TAGSTART 'table' 'border' ASSIGN NUMBER TAGEND;
TABLEENDTAG :       ENDTAG 'table' TAGEND;

ROWSTARTTAG :       TAGSTART 'tr' TAGEND;
ROWENDTAG :         ENDTAG 'tr' TAGEND;

ENTRYSTARTTAG :     TAGSTART 'td' TAGEND;
ENTRYENDTAG :       ENDTAG 'td' TAGEND;

OTHERCHARS: '!'|'#'|'$'|'%'|'\''|'('|')'|'*'|'+'|'-'|','|'.'|'/'|':'|';'|'?'|'@'|'['|']'|'^'|'_'|'`'|'|'|'~';

doc: HTMLSTARTTAG BODYSTARTTAG WS* table WS* BODYENDTAG HTMLENDTAG;
table: TABLESTARTTAG WS* row* WS* TABLEENDTAG;
row: ROWSTARTTAG WS* entry* WS* ROWENDTAG;
entry: ENTRYSTARTTAG WS* text WS* ENTRYENDTAG;
text: (LETTERS|NUMBER|WS|COMMANDSTART|COMMANDARGSTART|COMMANDARGEND|COMMENT|ASSIGN|QUOTE|OTHERCHARS)*;
