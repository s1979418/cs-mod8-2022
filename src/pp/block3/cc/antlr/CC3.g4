grammar CC3;
import CC3Vocab;

t : t0=t POWER t1=t     #exponent
  | t0=t PLUS t1=t      #plus
  | t0=t EQUALS t1=t    #assign
  | LPAR t0=t RPAR      #brackets
  | NUMBER              #num
  | BOOL                #bool
  | STRING              #str ;