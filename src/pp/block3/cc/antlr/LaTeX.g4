grammar LaTeX;
import LaTeXVocab;
@members{
     private int countAlignments(String text){
            int number = 0;
            for(char c:text.toCharArray()){
                if( c == 'l' || c=='c' || c=='r'){
                    number++;
                }
            }
            return number;
        }
     private static Pair<String,Integer> fixSeparators(String text){
             StringBuilder newText = new StringBuilder();
             int cols = 0;
             for(char c : text.toCharArray()){
                 if(c == '&'){
                     newText.append('|');
                     cols++;
                 }
                 else{
                     newText.append(c);
                 }
             }
             return new Pair<>(newText.toString(),cols);
         }
}

latex returns [String parsed, boolean valid]
    : WS* TABSTARTCOM ARGSTART align = alignment ARGEND tabl = table TABENDCOM WS* {
        $valid = $tabl.valid;
        $parsed = "-----------------------------------\n"+$tabl.parsed;
        if( ! ($align.columns == $tabl.columns)){
            $valid = false;
        }
        System.out.println("table.parsed = " + $parsed);
    };

alignment returns [int columns, char[] alignments]
    : tex = ALIGNMENT+ {
        $columns = countAlignments($tex.text);
        $alignments = $tex.text.toCharArray();
    };

table returns [String parsed, int columns, boolean valid]
    : rows = rowbuild+{
        $parsed = $rows.parsed;
        String check = $rows.parsed;
        System.out.println(check);

    };

rowbuild returns [String parsed, int columns, boolean valid]
    : row = tablerow ROWEND {
        $parsed = "|" + $row.parsed + "| \n";
        $columns = $row.columns;
        $valid = $row.valid;

    };
tablerow returns [String parsed, int columns, boolean valid]
    : t1=text WS* input = entry* {
        Pair<String,Integer> result = fixSeparators($t1.text+$input.text);
        $parsed = result.a;
        $columns = result.b;
        $valid = $input.valid && $t1.valid;

    };
entry returns [String parsed, boolean valid]
    : ENTRYSEP tex = text {
        $parsed = $tex.text;
        $valid = $tex.valid;

    };
text returns [String parsed, boolean valid]
    : tex = WORD* {
        $parsed = $tex.text;
        $valid = true;

    };

