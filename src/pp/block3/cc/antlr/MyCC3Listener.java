package pp.block3.cc.antlr;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import pp.block3.cc.antlr.CC3Parser.*;

public class MyCC3Listener extends CC3BaseListener {
    /**
     * Map storing the type attribute for all parse tree nodes.
     */
    private ParseTreeProperty<Type> types;

    /**
     * Initialises the calculator before using it to walk a tree.
     */
    public void init() {
        this.types = new ParseTreeProperty<Type>();
    }

    @Override
    public void exitStr(StrContext ctx) {
        set(ctx, Type.STR);
    }

    @Override
    public void exitBool(BoolContext ctx) {
        set(ctx, Type.BOOL);
    }

    @Override
    public void exitNum(NumContext ctx) {
        set(ctx, Type.NUM);
    }

    @Override
    public void exitPlus(PlusContext ctx) {
        if (getType(ctx.t0) == getType(ctx.t1)) {
            set(ctx, getType(ctx.t0));
        }

        else if (getType(ctx.t0) == Type.STR && getType(ctx.t1) == Type.STR){
               set(ctx,Type.STR);
            }
            else{
                set(ctx,Type.ERR);
            }
    }

    @Override
    public void exitBrackets(BracketsContext ctx) {
        set(ctx, getType(ctx.t0));
    }

    @Override
    public void exitExponent(ExponentContext ctx) {
        if (getType(ctx.t1) == Type.NUM){
            System.out.println("Evaluating a valid power");
            if(getType(ctx.t0) == Type.NUM){
                set(ctx,getType(ctx.t0));
            }
            else{
                set(ctx,Type.STR);
            }
        }
        else {
            System.out.println("Evaluating an invalid power");
            set(ctx,Type.ERR);
        }
    }

    @Override
    public void exitAssign(AssignContext ctx) {
        if(getType(ctx.t0) == getType(ctx.t1)) {
            set(ctx,Type.BOOL);
        }
        else{
            set(ctx,Type.ERR);
        }
    }

    /**
     * Sets the val attribute of a given node.
     */
    private void set(ParseTree node, Type type) {
        this.types.put(node, type);
    }

    /**
     * Retrieves the val attribute of a given node.
     */
    public Type getType(ParseTree node) {
        return this.types.get(node);
    }
}
