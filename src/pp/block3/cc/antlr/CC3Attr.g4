grammar CC3Attr;
import CC3Vocab;

@members {
    private int getValue(String text) {
        return Integer.parseInt(text);
    }

    private Boolean getBool(String text){
        return Boolean.parseBoolean(text);
    }
}

t returns [Type type]
     : t0=t POWER t1=t
       { if ($t1.type == Type.NUM){
            System.out.println("Evaluating a valid power");
            if($t0.type == Type.NUM){
               $type = $t0.type;
               }
            else{
                $type = Type.STR;
               }
       }
       else {
            System.out.println("Evaluating an invalid power");
            $type = Type.ERR;
       }

         }
     | t0=t PLUS t1=t
       { if($t0.type == Type.NUM && $t1.type == Type.NUM){
                   $type = $t0.type;
            }
         else if ($t0.type == Type.BOOL && $t1.type == Type.BOOL){
                  $type = $t0.type;
            }
         else if ($t0.type == Type.STR && $t1.type == Type.STR){
                                   $type = Type.STR;
            }
         else{
                             $type = Type.ERR;
            }

       }
     | | t0=t EQUALS t1=t
              { if ($t0.type == $t1.type){
                         $type = Type.BOOL;
                   }
                else{
                    $type = Type.ERR;
                }

              }
     | { System.out.println("Evaluating brackets"); }
       LPAR t0=t RPAR
       {$type = $t0.type;}
     | { System.out.println("Evaluating NUMBER"); }
       NUMBER
       { $type = Type.NUM;}
     | {System.out.println("Evaluating BOOLEAN");}
       BOOL
       { $type = Type.BOOL; }
     | {System.out.println("Evaluating STRING");}
       STRING
       { $type = Type.STR;}
     ;
