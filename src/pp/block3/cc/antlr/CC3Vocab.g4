lexer grammar CC3Vocab;

POWER  : '^';
PLUS   : '+';
EQUALS : '=';
LPAR   : '(';
RPAR   : ')';


BOOL : 'true' | 'false';
NUMBER : [0-9]+;
fragment CHAR   : [a-z]+;
STRING : (CHAR|NUMBER)+ ;


// ignore whitespace
WS : [ \t\n\r] -> skip;
