package pp.block3.cc.test;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Before;
import org.junit.Test;
import pp.block3.cc.symbol.DeclUseLexer;
import pp.block3.cc.symbol.DeclUseParser;
import pp.block3.cc.symbol.MyDeclUseListener;

import static org.junit.Assert.assertEquals;

public class DeclUseListenerTest {
	private final ParseTreeWalker walker = new ParseTreeWalker();
	private MyDeclUseListener listener;
	private final String validExample = "(D: aap (U: aap D: noot D: aap (U: noot ) (D: noot U: noot )) U: aap )";
	private final String brokenClosedScopes = "(D:aap (U: aap)))";
	private final String brokenOpenedScopes = "(D:aap ((U: aap))";
	private final String doubleDeclare = "(D: aap D: aap)";
	private final String useNoDeclare = "(U: noot)";
	private final String wrongScope = "(D: aap (D: noot) U: noot)";

	@Before
	public void initListener() {
		listener = new MyDeclUseListener();// construct an instance of your implementation
	}

	@Test
	public void test(){
		testErrorSize(0,validExample);
		//testErrorSize(1,invalidTokenException);
		testErrorSize(2,"("+useNoDeclare+doubleDeclare+")");

	}

	private void testErrorSize(int expected, String parse) {
		ParseTree tree = parseDeclUse(parse);
		this.walker.walk(this.listener, tree);
		assertEquals(expected, listener.getErrors().size());
	}

	private ParseTree parseDeclUse(String text) {
		CharStream chars = CharStreams.fromString(text);
		Lexer lexer = new DeclUseLexer(chars);
		TokenStream tokens = new CommonTokenStream(lexer);
		DeclUseParser parser = new DeclUseParser(tokens);
		return parser.program();
	}

}
