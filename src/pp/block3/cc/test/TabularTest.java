package pp.block3.cc.test;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Before;
import org.junit.Test;
import pp.block3.cc.antlr.LaTeXBaseListener;
import pp.block3.cc.antlr.LaTeXLexer;
import pp.block3.cc.antlr.LaTeXListener;
import pp.block3.cc.antlr.LaTeXParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TabularTest {
    private ParseTreeWalker walker;
    private LaTeXListener listener;

    @Before
    public void init(){
        this.walker = new ParseTreeWalker();
        this.listener = new LaTeXBaseListener();
    }
    private String getExample(String path){
        StringBuilder example = new StringBuilder();
        try {
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                example.append(data).append("\n");
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred for example 1 fetching.");
            e.printStackTrace();
        }
        return example.toString();
    }

    @Test
    public void test(){
        //example 1 test
        String path1 = "src/pp/block3/cc/tabular/tabular-1.tex";
        testGrammar(getExample(path1));

        //example 2 test
        String path2 = "src/pp/block3/cc/tabular/tabular-2.tex";
        testGrammar(getExample(path2));

        //example 3 test
        String path3 = "src/pp/block3/cc/tabular/tabular-3.tex";
        testGrammar(getExample(path3));

        //example 4 test
        String path4 = "src/pp/block3/cc/tabular/tabular-4.tex";
        testGrammar(getExample(path4));

        //example 5 test
        String path5 = "src/pp/block3/cc/tabular/tabular-5.tex";
        testGrammar(getExample(path5));
    }

    private void testGrammar(String parse) {
        ParseTree tree = parseLatex(parse);
        this.walker.walk(this.listener, tree);
    }

    private ParseTree parseLatex(String text) {
        CharStream chars = CharStreams.fromString(text);
        Lexer lexer = new LaTeXLexer(chars);
        TokenStream tokens = new CommonTokenStream(lexer);
        LaTeXParser parser = new LaTeXParser(tokens);
        return parser.latex();
    }



}
