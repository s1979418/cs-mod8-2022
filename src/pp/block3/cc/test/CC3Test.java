package pp.block3.cc.test;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Test;
import pp.block3.cc.antlr.*;

import static org.junit.Assert.assertEquals;

public class CC3Test {
	private final ParseTreeWalker walker = new ParseTreeWalker();
	private final MyCC3Listener CC3listen = new MyCC3Listener();

	@Test
	public void test() {
		test(Type.BOOL, "false");
		test(Type.NUM, "8");
		test(Type.STR, "hello");
		test(Type.STR, "hi ^ 4");
		test(Type.NUM, "5 ^3");
		test(Type.BOOL, "true + false");
		test(Type.STR, "hello + world");
		test(Type.BOOL, "5 = 3");
		test(Type.BOOL, "hello = hi");
		test(Type.ERR, "hello = false");
		test(Type.ERR, "3 ^ two");
	}

	private void test(Type expected, String expr) {
		assertEquals(expected, parseCCAttr(expr).type);
		ParseTree tree = parseCalc(expr);
		this.CC3listen.init();
		this.walker.walk(this.CC3listen, tree);
		assertEquals(expected, this.CC3listen.getType(tree));
	}

	private ParseTree parseCalc(String text) {
		CharStream chars = CharStreams.fromString(text);
		Lexer lexer = new CC3Lexer(chars);
		TokenStream tokens = new CommonTokenStream(lexer);
		CC3Parser parser = new CC3Parser(tokens);
		return parser.t();
	}

	private CC3AttrParser.TContext parseCCAttr(String text) {
		CharStream chars = CharStreams.fromString(text);
		Lexer lexer = new CC3AttrLexer(chars);
		TokenStream tokens = new CommonTokenStream(lexer);
		CC3AttrParser parser = new CC3AttrParser(tokens);
		return parser.t();
	}
}
