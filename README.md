# cs-mod8-2022
This project is a log of my answers to exercises of Module 8 Programming Paradigms of the TCS bachelor at the University of Twente in the academic year 2021-2022. 



## Authors and acknowledgment
Exercise solutions: Kiona Bijker

Exercises and pre-made files:
Remco Abraham,
Martijn Bakker,
Martijn Bastiaan,
Pim van den Broek,
Marco Gerards,
Arnd Hartmanns,
Marieke Huisman,
Sebastiaan Joosten,
Jan Kuper,
Raul E. Monti ,
Jaco van de Pol,
Arend Rensink,
Rick de Vries

Used IDEs : Visual Studio Code, IntelliJ IDEA 2022.1

All pre-made files and exercises belong to the University of Twente

## Project status
